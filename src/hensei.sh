#!/usr/bin/env bash
#"""
# Actions in Teitoku room
#"""

# dependencies
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/func.sh"


hensei_fleet() {
  #"""
  # Select fleet
  #
  # Args:
  #   $1 (int): 1~4
  #"""
  echo "$(date +%T) [Debug] hensei_fleet($@)..."
  # showing ships takes more time than $T_CLICK_INSTANT
  zmcs $((95 + $1 * 31)) 8 114 10  1 $T_CLICK_ANIMATE
}


hensei_dismiss() {
  #"""
  # dismiss except flagship
  #"""
  echo "$(date +%T) [Debug] hensei_dismiss()..."
  zmcs 394 40 118 3  1 $T_CLICK_API
}


hensei_select_order() {
  #"""
  # Select (switch) hensei order
  #"""
  echo "$(date +%T) [Debug] hensei_select_order()..."
  zmcs 755 35 104 13  1 $T_CLICK_INSTANT
}


hensei_select_ship() {
  #"""
  # Select a ship in hensei interface.
  # See ``${SCRIPTHOME}/mass-kira.txt.template`` for the definition
  # of positional ship ID .
  #
  # Args:
  #   $1 (int): ship positional ID (XXYY)
  #"""

  echo "$(date +%T) [Info] hensei_select_ship($@)..."

  local page=$(( $1 / 100 ))
  local item=$(( $1 % 100 ))

  # back to the first page
  zmcs 427 17 445 10 1 $T_CLICK_INSTANT

  # special cases (LAST, 1~5)
  if (($page == 99)); then
      zmcs 712 20 446 7 1 $T_CLICK_INSTANT

  elif (($page == 1)); then
      :

  elif (($page == 2)); then
      zmcs 538 6 444 12 1 $T_CLICK_INSTANT

  elif (($page == 3)); then
      zmcs 574 6 444 11 1 $T_CLICK_INSTANT

  elif (($page == 4)); then
      zmcs 609 6 444 10 1 $T_CLICK_INSTANT

  elif (($page == 5)); then
      zmcs 645 5 446 10 1 $T_CLICK_INSTANT

  # generic case (6 to LAST-1)
  else
      # how many times clicking the "advance 5 pages" arrow
      local fwd5=$(( ($page - 1) / 5 ))
      # how many times clicking the next page
      local fwd1=$(( $page - 1 - $fwd5 * 5 ))

      local i
      for (( i = 1; i <= $fwd5; i++ )); do
          zmcs 676 13 444 10 1 $T_CLICK_INSTANT
      done

      for (( i = 1; i <= $fwd1; i++ )); do
          zmcs 609 6 444 10 1 $T_CLICK_INSTANT
      done
  fi

  ## select ship
  # y = 158~410 for $item=1~10
  if (($item >= 1)) && (($item <= 10)); then
    zmcs 394 140 $((130 + $item * 28)) 14  1 $T_CLICK_ANIMATE
  else
    echo "$(date +%T) [Error] wrong ship list item number $item"
    exit 0
  fi

  ## confirm ("henkou" button)
  zmcs 650 82 430 26  1 $T_CLICK_API
}


hensei_henkou() {
  #"""
  # Hit "henkou" (confirm) in ship selection interface
  # See ``${SCRIPTHOME}/mass-kira.txt.template`` for the definition
  # of positional ship ID .
  #
  # Args:
  #   $1 (int): hensei slot 1~6
  #   $2 (int): ship positional ID (XXYY)
  #"""
  echo "$(date +%T) [Debug] hensei_henkou($@)..."

  local xadd=$(( ($1 - 1) % 2 ))
  local yadd=$(( ($1 - 1) / 2 ))

  # select slot
  zmcs $((376 + $xadd * 346)) 55  $((208 + $yadd * 114)) 15  1 $T_CLICK_ANIMATE

  # replace ship
  hensei_select_ship $2
}

##############
# S/L Tabs
##############


hensei_record() {
  #"""
  # Save hensei record
  #"""
  echo "$(date +%T) [Debug] hensei_record()..."
  zmcs 51 42 348 12 1 $T_CLICK_ANIMATE
}


hensei_expand() {
  #"""
  # Load hensei record
  #"""
  echo "$(date +%T) [Debug] hensei_expand()..."
  zmcs 52 42 379 9  1 $T_CLICK_ANIMATE
}


hensei_sl_select() {
  #"""
  # Select record slot (common to Save/Load)
  #
  # Args:
  #   $1 (int): which slot to click (1~3)
  #"""
  echo "$(date +%T) [Debug] hensei_sl_select($@)..."
  zmcs 287 22 $((142 + $1 * 53)) 8  1 $T_CLICK_API
}


hensei_sl_back() {
  #"""
  # Leave S/L interface (common to S/L)
  #"""
  echo "$(date +%T) [Debug] hensei_sl_back()..."
  zmcs 122 32 434 14 1 $T_CLICK_INSTANT
}
