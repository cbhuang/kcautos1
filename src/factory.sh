#!/usr/bin/env bash
#"""
# Factory Actions
#"""

# dependencies
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/func.sh"

#"""
# build
#"""

build_slot() {
  #"""
  # Click build slot
  #
  # Args:
  #   $1 (int): which slot to build (I have only 2)
  #"""
  echo "$(date +%T) [Debug] build_slot($@)..."
  zmcs 590 50 $(( 95 + $1 * 78 )) 15 1 $T_CLICK_API
}


build_set_single() {
  #"""
  # Set single resource at build interface
  #
  # Args:
  #   $1  fuel/ammo/steel/baux : set coordinate to hit
  #   $2  10~300 : amount
  #"""
  echo "$(date +%T) [Debug] build_set_single($@)..."

  ## set local coordinate to hit
  local xadd
  local yadd
  if [ $1 == "fuel" ]; then
    xadd=0
    yadd=0
  elif [ $1 == "ammo" ]; then
    xadd=0
    yadd=129
  elif [ $1 == "steel" ]; then
    xadd=228
    yadd=0
  elif [ $1 == "baux" ] || [ $1 == "bauxite" ]; then
    xadd=228
    yadd=129
  else
    echo "$(date +%T) [Error] Wrong resource type: $1"
    exit 1
  fi

  ## compute digits
  if [ $(chk_build_resource $2) -eq 0 ]; then
    local hundreds=$(( ($2 - 30) / 100 ))
    local tens=$(( ( ($2 - 30) % 100 ) / 10 ))
    local ones=$(( $2 % 10 ))
  else
    local hundreds=0
    local tens=0
    local ones=0
    echo "$(date +%T) [Debug] Bad resource amount: $2, set to 30"
  fi

  ## hit
  # ones
  local i
  for (( i=1; i <= $ones; i++ )); do
    zmcs $((358 + $xadd)) 6 $((153 + $yadd)) 4 1 $T_CLICK_INSTANT
  done

  # hit tens
  for (( i=1; i <= $tens; i++ )); do
    zmcs $((488 + $xadd)) 6 $((132 + $yadd)) 6 1 $T_CLICK_INSTANT
  done

  # hit hundreds
  for (( i=1; i <= $hundreds; i++ )); do
    zmcs $((488 + $xadd)) 6 $((160 + $yadd)) 6 1 $T_CLICK_INSTANT
  done
}


build_set_recipe() {
  #"""
  # Set all 4 resource at build interface
  #
  # Args:
  #   $1..$4 (int): (fuel, ammo, steel, baux) 10~300
  #"""

  echo "$(date +%T) [Debug] build_set_recipe($@)..."
  build_set_single fuel $1
  build_set_single ammo $2
  build_set_single steel $3
  build_set_single baux $4
}


build_set_fast() {
  #"""
  # Click fast construction button
  #"""
  echo "$(date +%T) [Debug] build_set_fast()..."
  zmcs 417 64 391 11 1 $T_CLICK_ANIMATE
}


build_do() {
  #"""
  # Click "build" after recipe is set
  #"""
  echo "$(date +%T) [Debug] build_do()..."
  zmcs 646 124 428 29 1 $T_CLICK_API
}


build_launch() {
  #"""
  # Take a finished ship out of dock
  #
  # Args:
  #   $1  1~4: slot to launch
  #"""
  echo "$(date +%T) [Debug] build_launch($@)..."
  build_slot $1
  sleep 10
  click_anywhere 1800
}


build() {
  #"""
  # Integrated action series
  #
  # Args:
  #   $1 (int): slot (1~4)
  #   $2 (char): high speed build (y/n)
  #   $3~$6 (int): recipe (optional)
  #"""

  echo "$(date +%T) [Info] build($@) begins..."
  build_slot $1
  if [ $(chk_yes $2) -eq 0 ]; then
    build_set_fast
  fi
  build_set_recipe $3 $4 $5 $6
  build_do
  echo "$(date +%T) [Info] build($@) done!"
}


#"""
# disintegrate
#"""


factory_disintegrate() {
  #"""
  # On factory menu, hit disintegrate button
  #"""
  echo "$(date +%T) [Debug] factory_disintegrate()..."
  zmcs 170 120 240  30 1 $T_CLICK_ANIMATE
}


disintegrate_top() {
  #"""
  # Disintegrate top N ships
  #
  # Args:
  #   $1 (int): ships to disintegrate
  #"""
  echo "$(date +%T) [Info] disintegrate_top($@)..."

  if [ -z "$1" ]; then
    local N=1
  else
    local N=$1
  fi

  local i=1
  while (( i <= N )); do
    echo "$(date +%T) [Debug]   disintegrate $i of $1..."
    # select top (assume already sorted by "new")
    zmcs 210 300 130 10 1 $T_CLICK_ANIMATE
    # hit disintegrate
    zmcs 650  90 420 30 1 $T_CLICK_API_LONG
    (( i += 1 ))
  done
}

disintegrate_equip_switch() {
  #"""
  # "Reserve/disintegrate equipments" switch (appears when a ship is checked)
  #"""
  echo "$(date +%T) [Debug] disintegrate_equip_switch"
  zmcs 742 45 102 9 1 $T_CLICK_ANIMATE
}


#"""
# development
#"""


factory_develop() {
  #"""
  # On factory menu, hit development button
  #"""

  echo "$(date +%T) [Debug] factory_develop()..."
  # move elsewhere to ensure development menu shows (bug?)
  #zms 20 20 20 20 432
  # click development button
  zmcs 140 160 320  35 1 $T_CLICK_ANIMATE
}


develop_set_single() {
  #"""
  # Set single resource of development
  #
  # Args:
  #   $1 (str): fuel/ammo/steel/baux
  #   $2 (int): 10~300
  #"""

  echo "$(date +%T) [Debug] develop_set_single($@) begins..."

  # set position
  local xadd
  local yadd
  if [ $1 == "fuel" ]; then
    xadd=0
    yadd=0
  elif [ $1 == "ammo" ]; then
    xadd=0
    yadd=131
  elif [ $1 == "steel" ]; then
    xadd=230
    yadd=0
  elif [ $1 == "baux" ] || [ $1 == "bauxite" ]; then
    xadd=230
    yadd=131
  else
    echo "$(date +%T) [Error] Wrong resource type $1"
    exit 1
  fi

  # compute digits
  if [ $(chk_develop_resource $2) -eq 0 ]; then
    local hundreds=$(( ($2 -10) / 100 ))
    local tens=$(( ( ($2 -10) % 100) / 10 ))
    local ones=$(( $2 % 10 ))
  else
    local hundreds=0
    local tens=0
    local ones=0
    echo "$(date +%T) [Debug] Wrong resource amount $2 changed to 10"
  fi

  # hit
  # reset
  zmcs $((438 + $xadd)) 58 $((187 + $yadd)) 9 1 $T_CLICK_INSTANT
  # ones
  local i
  for (( i=1; i <= $ones; i++ )); do
    zmcs $((357 + $xadd)) 8 $((150 + $yadd)) 5 1 $T_CLICK_INSTANT
  done

  # hit tens
  for (( i=1; i <= $tens; i++ )); do
    zmcs $((488 + $xadd)) 6 $((131 + $yadd)) 6 1 $T_CLICK_INSTANT
  done

  # hit hundreds
  for (( i=1; i <= $hundreds; i++ )); do
    zmcs $((488 + $xadd)) 6 $((160 + $yadd)) 6 1 $T_CLICK_INSTANT
  done

  echo "$(date +%T) [Debug] develop_set_single($@) done!"
}



develop_set_recipe() {
  #"""
  # Set 4 resources at once
  #
  # Args:
  #   $1~$4 (int): recipe
  #"""
  echo "$(date +%T) [Debug] develop_set_recipe($@)...."
  develop_set_single fuel $1
  develop_set_single ammo $2
  develop_set_single steel $3
  develop_set_single baux $4
}


develop_do() {
  #"""
  # Do and confirm result
  #""
  echo "$(date +%T) [Debug] develop_do()..."
  zmcs 650 110 430  20 1 11500
  click_anywhere $T_CLICK_ANIMATE
}


develop() {
  #"""
  # Integrated action development
  #
  # Args:
  #   $1~$4 (int): recipe
  #   $5 (int): times to repeat
  #"""
  echo "$(date +%T) [Debug] develop($@) begins..."

  local N
  if [ -z "$5" ]; then
    echo "$(date +%T) [Debug] Repeat times missing, set to 1"
    N=1
  elif (( $5 >= 1 )); then
    N=$5
  else
    echo "$(date +%T) [Error] Wrong times parameter: $5"
    exit 1
  fi

  local i=1
  while (( i <= N )); do
    echo "$(date +%T) [Debug] Do development...($i of $N)"
    factory_develop
    if (( i == 1 )); then
      develop_set_recipe $1 $2 $3 $4
    fi
    develop_do
    (( i += 1 ))
  done
  echo "$(date +%T) [Debug] develop($@) done!"
}
