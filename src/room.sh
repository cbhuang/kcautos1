#!/usr/bin/env bash
#"""
# Teitoku room operations
#"""

# dependencies
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/func.sh"



room_spe() {
  #"""
  # Hit sortie button (goes to "spe" interface)
  #"""
  echo "$(date +%T) [Debug] room_spe()..."
  zmcs 160 70 225 70  1 $T_CLICK_ANIMATE
}
room_sortie() {
  #"""
  # Alias of room_spe
  #"""
  room_spe
}


room_factory() {
  echo "$(date +%T) [Debug] room_factory()..."
  zmcs 250 40 340 40  1 $T_CLICK_API
}


room_hensei() {
  echo "$(date +%T) [Debug] room_hensei()..."
  zmcs 177 46 112 50  1 $T_CLICK_API
}


room_refit() {
  echo "$(date +%T) [Debug] room_refit()..."
  zmcs 291 51 198 42 1 $T_CLICK_ANIMATE
}


room_docking() {
  echo "$(date +%T) [Debug] room_docking()..."
  zmcs 98 47 340 46 1 $T_CLICK_API
}


room_supply() {
  echo "$(date +%T) [Debug] room_supply()..."
  zmcs 52 45 204 38 1 $T_CLICK_ANIMATE
}


room_quest() {
  echo "$(date +%T) [Debug] room_quest()..."
  # Bug in 1st seq: occationally one must click 2+ time to enter Quest
  # zmcs 535 50 40 15 1 222
  # zmcs 535 50 40 15 1 333
  # zmcs 535 50 40 15 1 444
  # enter quest || dismiss Ooyodo
  zmcs 535 50 40 15 1 $T_CLICK_API
  # dismiss Ooyodo || safeclick
  zmcs 535 50 40 15 1 $T_CLICK_API
}
