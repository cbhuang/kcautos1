#!/usr/bin/env bash
#"""
# Battle actions (AFTER entering maps).
#
# Functions before entering maps is collected within ``sortie.sh``
#
# TODO:
#     * better factorized
#     * consider rename formation_ , combat_ ,...
#"""

# project settings & function backend
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/func.sh"

#"""
# Common Timing Functions
# ===================================
#"""


combat_sleep_day() {
  #"""
  # Wait for day combat to end.
  #
  # Assume single fleet to single fleet combat.
  #
  # Args:
  #   $1  0/1: jet assult
  #   $2  0/1: aerial combat
  #   $3  0/1: expedition support fire
  #   $4  0~6: opening anti-SS attack (incl. enemy side)
  #   $5  0/1: opening torpedo salvo
  #   $6  0/1: harsh 2nd round shelling correction
  #   $7  1~6: estimated maximum number of medium damage
  #   $8  ms   case-specific correction (milliseconds)
  #
  # NOTE: we assume 6 vessels (therefore too conservative for 1-1 and 1-5)
  #"""

  echo "$(date +%T) [Debug] combat_sleep_day($@) begins..."

  if [ -z "$7" ]; then
    echo "$(date +%T) [Debug] No # of mid dmg ships, set 2"
    local n_middmg=2
  else
    local n_middmg=$1
  fi

  if [ -z "$8" ]; then
    local t_arbi=0
  else
    local t_arbi=$8
  fi

  local t=$(( ( T_COMBAT_DAY + \
                T_COMBAT_JETASSULT * $1 + \
                T_COMBAT_AERIAL * $2 + \
                T_COMBAT_SUPPORTFIRE * $3 + \
                T_COMBAT_ANTISS * $4 + \
                T_TORPEDO * $5 + \
                T_COMBAT_2NDRND * $6 + \
                T_COMBAT_MIDDMG * $n_middmg + \
                $t_arbi ) / 1000 ))
  echo "$(date +%T) [Debug] sleep $t ..."
  sleep $t
  echo "$(date +%T) [Debug] combat_sleep_day($@) done!"
}


combat_sleep_night() {
  #"""
  # Wait for night combat to end
  #
  # Args:
  #   $1  number of medium damage 0~6 (default 2. That's already super unlucky.)
  #"""
  echo "$(date +%T) [Debug] combat_sleep_night($@) begins..."

  if [ -z "$1" ]; then
    echo "$(date +%T) [Debug] No # of mid dmg ships, set 3"
    local n_middmg=2
  else
    local n_middmg=$1
  fi

  local t=$(( ( T_COMBAT_NIGHT + 5000 * n_middmg ) / 1000 ))
  echo "$(date +%T) [Debug] sleep $t ..."
  sleep $t
  echo "$(date +%T) [Debug] combat_sleep_night($@) done!"
}


combat_sleep_night_6v6() {
  #"""
  # Sleep time for full night battle
  #
  # Args:
  #   $1  number of medium damage 0~6 (default 2. That's already super unlucky.)
  #"""
  echo "$(date +%T) [Debug] combat_sleep_night_6v6($@) begins..."

  if [ -z "$1" ] || [[ $1 -le 2 ]]; then
    echo "$(date +%T) [Debug] # of mid dmg ships set to 2"
    local i=0
  else
    local i=$(( $1 - 2 ))
  fi

  local t=$(( ( T_COMBAT_NIGHT_6v6 + i * T_COMBAT_MIDDMG ) / 1000 ))
  echo "$(date +%T) [Debug] sleep $t ..."
  sleep $t
  echo "$(date +%T) [Debug] combat_sleep_night_6v6($@) done!"
}


#"""
# formation
# ===================================
#"""

combat_formation() {
  #"""
  # Choose single fleet formation
  #
  # Args:
  #     $1 (int): formation 1~6
  #               1 = Line Ahead
  #               2 = Double Line
  #               3 = Diamond
  #               4 = Echelon
  #               5 = Line Abreast
  #               6 = Alert
  #"""
  echo "$(date +%T) [Debug] formation($@)..."

  # has formation 6
  if [ $(chk_yes $FORMATION6_EXIST) -eq 0 ]; then
    if (( $1 == 1 )); then
      zmcs 401 90 174 19 1 $T_CLICK_INSTANT
    elif (( $1 == 2 )); then
      zmcs 531 90 174 19 1 $T_CLICK_INSTANT
    elif (( $1 == 3 )); then
      zmcs 663 90 174 19 1 $T_CLICK_INSTANT
    elif (( $1 == 4 )); then
      zmcs 401 90 333 19 1 $T_CLICK_INSTANT
    elif (( $1 == 5 )); then
      zmcs 531 90 332 19 1 $T_CLICK_INSTANT
    elif (( $1 == 6 )); then
      zmcs 663 90 332 19 1 $T_CLICK_INSTANT
    else
      echo "$(date +%T) [Error] invalid argument $1"
    fi
  # no formation 6
  else
    if (( $1 == 1 )); then
      zmcs 401 90 174 19 1 $T_CLICK_INSTANT
    elif (( $1 == 2 )); then
      zmcs 531 90 174 19 1 $T_CLICK_INSTANT
    elif (( $1 == 3 )); then
      zmcs 663 90 174 19 1 $T_CLICK_INSTANT
    elif (( $1 == 4 )); then
      zmcs 467 90 332 19 1 $T_CLICK_INSTANT
    elif (( $1 == 5 )); then
      zmcs 599 90 332 19 1 $T_CLICK_INSTANT
    else
      echo "$(date +%T) [Error] invalid argument $1"
    fi
  fi
}

#"""
# post-combat actions
# ========================
#"""

combat_advance() {
  #"""
  # press advance button
  #
  # Args:
  #   $1  y/n  need to hit compass or not (default y)
  #"""
  echo "$(date +%T) [Info] combat_advance($@) begins...."

  if [ $(chk_no $1) -eq 0 ]; then
    echo "$(date +%T) [Debug]   No compass follow-up!"
    local t=3600
  else
    local t=7200
  fi

  zmcs  264 52 222 46 1 $t
  echo "$(date +%T) [Debug] combat_advance($@) done!"
}


combat_retreat() {
  #"""
  # Retreat back to room
  #"""
  echo "$(date +%T) [Info] combat_retreat()..."
  zmcs 482 48 219 41 1 $T_CLICK_API_LONG
}


combat_report() {
  #"""
  # standard post-combat actions (day S or after yasen)
  #"""

  echo "$(date +%T) [Info] combat_report() begins..."

  echo "$(date +%T) [Debug] confirm rank"
  click_safe $T_COMBAT_RANK_EXP

  echo "$(date +%T) [Debug] confirm exp"
  click_safe $T_COMBAT_EXP_DROP

  echo "$(date +%T) [Debug] confirm drop || click_safe() (day S)"
  click_safe $T_COMBAT_DROP_ROOM
  # may be back to room, or at advancement choice

  echo "$(date +%T) [Info] combat_report() done!"
}


combat_report_nodrop() {
  #"""
  # standard post-combat actions for no-drop nodes (e.g. air raid)
  #"""

  echo "$(date +%T) [Info] combat_report_nodrop() begins..."

  echo "$(date +%T) [Debug] confirm rank"
  click_safe $T_COMBAT_RANK_EXP

  echo "$(date +%T) [Debug] confirm exp"
  click_safe $T_COMBAT_DROP_ADV

  echo "$(date +%T) [Info] combat_report_nodrop() done!"
}


combat_report_practice() {
  #"""
  # standard post-combat actions for practice
  #"""

  echo "$(date +%T) [Info] combat_report_practice() begins..."

  echo "$(date +%T) [Debug] confirm rank"
  click_safe $T_COMBAT_RANK_EXP

  echo "$(date +%T) [Debug] confirm exp"
  click_safe $T_COMBAT_DROP_ROOM

  echo "$(date +%T) [Info] combat_report_practice() done!"
}

#"""
# night combat (yasen)
# =============================
#"""


combat_yasen_button_yes() {
  #"""
  # hit yasen button
  #"""
  zmcs 460 50 220 45  1 $T_CLICK_INSTANT
}


combat_yasen_button_no() {
  #"""
  # hit no-yasen button
  # NOTE: no sleep time because of combined fleet possibility
  #"""
  zmcs 260 50 220 45  1 $T_CLICK_INSTANT
}


combat_yasen() {
  #"""
  # Enter night combat and receive combat report
  #
  # Args:
  #   $1 (int): assumed max number of heavy damage (default 3, blank is ok)
  #"""

  echo "$(date +%T) [Info] combat_yasen($@) begins..."
  # hit yasen button
  combat_yasen_button_yes
  combat_sleep_night $1
  combat_report
  echo "$(date +%T) [Info] combat_yasen($@) done!"
}


combat_no_yasen() {
  #"""
  # hit no yasen button and accept report
  #"""

  echo "$(date +%T) [Info] combat_no_yasen() begins..."
  # hit no yasen
  combat_yasen_button_no
  sleep $(( $T_COMBAT_END_RANK / 1000 ))

  combat_report
  echo "$(date +%T) [Info] combat_no_yasen() done!"
}


combat_no_yasen_unsure() {
  #"""
  # When unsure of yasen appears or not, always choose no yasen
  # and accept report.
  #"""

  echo "$(date +%T) [Info] combat_no_yasen_unsure() begins..."

  echo "$(date +%T) [Debug] hit no_yasen || confirm rank (day S)"
  combat_yasen_button_no
  sleep $(( $T_COMBAT_END_RANK / 1000 ))

  echo "$(date +%T) [Debug] confirm rank || exp (day S)"
  click_safe $T_COMBAT_END_RANK

  echo "$(date +%T) [Debug] confirm exp || drop (day S) || click_safe() (day S, no drop)"
  click_safe $T_COMBAT_EXP_DROP

  echo "$(date +%T) [Debug] confirm drop || click_safe() (day S)"
  click_safe $T_COMBAT_DROP_ROOM

  echo "$(date +%T) [Info] combat_no_yasen_unsure() done!"
}


combat_no_yasen_no_drop() {
  #"""
  # When you're sure (1) must hit no_yasen (2) no drop
  #"""
  echo "$(date +%T) [Info] combat_no_yasen_no_drop() begins..."

  echo "$(date +%T) [Debug] hit no_yasen"
  combat_yasen_button_no
  sleep $(( $T_COMBAT_END_RANK / 1000 ))

  echo "$(date +%T) [Debug] confirm rank"
  click_safe $T_COMBAT_RANK_EXP

  echo "$(date +%T) [Debug] confirm exp"
  click_safe $T_CLICK_ANIMATE

  echo "$(date +%T) [Info] combat_no_yasen_no_drop() done!"
}


combat_yasen_unsure() {
  #"""
  # When unsure of yasen appears or not, always choose YASEN
  # and accept report.
  #
  # Args:
  #   $1 (int): assumed max number of medium damage (default to 3, empty ok)
  #"""

  echo "$(date +%T) [Info] combat_yasen_unsure($@) begins..."

  echo "$(date +%T) [Debug] hit yasen || confirm rank (day S)"
  combat_yasen_button_yes
  sleep $(( $T_COMBAT_END_RANK / 1000 ))
  combat_sleep_night $1

  echo "$(date +%T) [Debug] confirm rank || confirm exp (day S)"
  click_safe $T_COMBAT_END_RANK

  echo "$(date +%T) [Debug] confirm exp || confirm drop (day S) || click_safe() (day S, no drop)"
  click_safe $T_COMBAT_EXP_DROP

  echo "$(date +%T) [Debug] confirm drop || click_safe() (day S)"
  click_safe $T_COMBAT_DROP_ROOM

  echo "$(date +%T) [Info] combat_yasen_unsure($@) done!"
}


combat_yasen_unsure_practice() {
  #"""
  # When unsure of yasen appears or not, always choose YASEN
  # and accept report. Practice mode (no drop).
  #
  # Assume 4 heavy dmg (should be enough).
  #"""

  echo "$(date +%T) [Info] combat_yasen_unsure_practice($@) begins..."

  echo "$(date +%T) [Debug] hit yasen || confirm rank (day S)"
  combat_yasen_button_yes
  sleep $(( $T_COMBAT_END_RANK / 1000 ))
  combat_sleep_night_6v6 4
  combat_report_practice

  echo "$(date +%T) [Info] combat_yasen_unsure_practice($@) done!"
}


#"""
# User Interaction
# ===============================
#
# Functions in this section should end with _choose
#"""


combat_formation_choose() {
  #"""
  # Ask for formation
  #"""

  local res
  read_safe res "Enter formation when the choice shows (1~5)"
  combat_formation $res
}


combat_yasen_choose() {
  #"""
  # Ask yasen or not
  #"""

  local flag
  read_safe flag "yasen? (n/y/s, s=day S)"

  if [ $(chk_yes $flag) -eq 0 ]; then
    combat_yasen
  elif [ $(chk_no $flag) -eq 0 ]; then
    combat_no_yasen
  elif [ "$flag" == "s" ]; then
    combat_report
  else
    combat_no_yasen_unsure
  fi
}


combat_advance_choose() {
  #"""
  # Ask advance or not (DEPRECATED)
  #"""

  local flag
  read_safe flag "Advance? (n/y/b, b=already back to room)"

  if [ $(chk_yes $flag) -eq 0 ]; then
    echo "$(date +%T) [Info] User advancing..."
    combat_advance
  elif [ "$flag" == "b" ]; then
    echo "$(date +%T) [Info] Script ended by combat_advance_choose()!"
    exit 0
  else
    combat_retreat
    echo "$(date +%T) [Info] Script ended by combat_advance_choose()!"
    exit 0
  fi
}


combat_next_choose() {
  #"""
  # Ask for next action after combat report
  #"""

  local res
  read_safe res "Enter next action when the chioce shows (n=retreat)/y=advance/b=back to room)"

  combat_report

  if [ "$res" == "n" ]; then
    combat_retreat
  elif [ "$res" == "y" ]; then
    combat_advance
  elif [ "$res" == "b" ]; then
    echo "$(date +%T) [Debug] back to room, doing nothing!"
  else
    echo "$(date +%T) [Error] Bad input: $res"
    exit 1
  fi
}


combat_single() {
  #"""
  # generic interactive combat script (still experimental!)
  #
  # States:
  #   (map state, may need Teitoku click) ->
  #     formation choice(1~5) ->
  #   (combat state, may need Teitoku click) ->
  #     confirm rank & decide next step ->
  #   (map state / room state)
  #"""

  echo "$(date +%T) [Debug] combat_single() begins..."

  # map state
  echo "$(date +%T) [Debug] entered map state"
  # map to combat transition
  combat_formation_choose

  # combat state
  echo "$(date +%T) [Debug] entered combat state"
  # combat to map transition
  combat_next_choose

  echo "$(date +%T) [Info] combat_single() done!"
}
