#!/usr/bin/env bash
#"""
# Expedition Action Functions
#
# NOTE: Action to receive fleets, e.g. expd_receive(), is in action.sh
# because it DOES NOT run in expedition interface ONLY. It involves
# sidebar and room actions and is therefore compound.
#"""

# dependencies
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/func.sh"


#"""
# Functions
#"""


expd_id_get_seaarea() {
  #"""
  # Calculate seaarea from expd_id
  #
  # Args:
  #   $1 (expd_id):
  #
  # Returns:
  #   int: seaarea (01-08/A1~A3: 1, 09-16/B1: 2,...)
  #"""

  if [ $(chk_expd_id $1) -eq 1 ]; then
    echo "$(date +%T) [Error] Bad expd id: $1"
    exit 1
  fi

  # numeric id
  if [ $(chk_num $1) -eq 0 ]; then
    echo $(( ($1 - 1) / 8 + 1 ))
  # text id
  elif [ "$(ltnum_get_lt $1)" == "A" ]; then
    echo 1
  elif [ "$(ltnum_get_lt $1)" == "B" ]; then
    echo 2
  # not implemented
  else
    echo "$(date +%T) [Error] expd_id not implemented: $1"
    exit 1
  fi
}


expd_id_get_time() {
  #"""
  # Get expedition time from expd_id
  #
  # Args:
  #   $1 (expd_id)
  #
  # Returns:
  #   int: seconds
  #"""

  # filter out illegal id
  if [ $(chk_expd_id $1) -eq 1 ]; then
    exit 1
  fi

  # numeric
  if [ $(chk_num $1) -eq 0 ]; then
    echo ${T_EXPD[$1]}
  # text
  else
    # sparse is better than dense
    local letter="$(ltnum_get_lt $1)"
    local num="$(ltnum_get_num $1)"
    local arr_elem="T_EXPD_${letter}[${num}]"
    echo ${!arr_elem}
  fi
}


expd_sleep_warn_sidebar() {
  #"""
  # Sleep and warn "make sidevar visible"
  #
  # Args:
  #     $1 (int): seconds to sleep
  #"""
  echo "  *** ~${1}s to fleet return. Make sidebar visible! ***"
  sleep $1
}


#"""
# Top Bar
#"""

expd_sortie() {
  echo "$(date +%T) [Debug] expd_sortie()..."
  zmcs 314 62 105 14  1 $T_CLICK_API_ANIMATE
}

expd_practice() {
  echo "$(date +%T) [Debug] expd_sortie()..."
  zmcs 395 68 104 15  1 $T_CLICK_API_ANIMATE
}


#"""
# Expedition Buttons
#"""
# NOTE: positions of seaarea buttons must be enumerated after 2017.10.19

# seaarea1
expd_seaarea1() {
  echo "$(date +%T) [Info] expd_seaarea1()..."
  zmcs 115 27 424 22 1 $T_CLICK_INSTANT
}

expd_seaarea1_next() {
  echo "$(date +%T) [Info] expd_seaarea1_next()..."
  zmcs 304 18 404 9 1 $T_CLICK_INSTANT
}

expd_seaarea1_prev() {
  echo "$(date +%T) [Info] expd_seaarea1_prev()..."
  zmcs 303 18 134 12 1 $T_CLICK_INSTANT
}

# seaarea 2
expd_seaarea2() {
  echo "$(date +%T) [Info] expd_seaarea2()..."
  zmcs 156 28 424 21 1 $T_CLICK_INSTANT
}

expd_seaarea2_next() {
  echo "$(date +%T) [Info] expd_seaarea2_next()..."
  zmcs 304 18 404 9 1 $T_CLICK_INSTANT
}

expd_seaarea2_prev() {
  echo "$(date +%T) [Info] expd_seaarea2_prev()..."
  zmcs 303 18 134 12 1 $T_CLICK_INSTANT
}

# seaarea 3,7,4,5,6
expd_seaarea3() {
  echo "$(date +%T) [Info] expd_seaarea3()..."
  zmcs 198 28 424 19 1 $T_CLICK_INSTANT
}

expd_seaarea7() {
  echo "$(date +%T) [Info] expd_seaarea7()..."
  zmcs 238 30 424 19 1 $T_CLICK_INSTANT
}

expd_seaarea4() {
  echo "$(date +%T) [Info] expd_seaarea4()..."
  zmcs 279 30 435 10 1 $T_CLICK_INSTANT
}

expd_seaarea5() {
  echo "$(date +%T) [Info] expd_seaarea5()..."
  zmcs 321 30 435 11 1 $T_CLICK_INSTANT
}

expd_seaarea6() {
  echo "$(date +%T) [Info] expd_seaarea6()..."
  zmcs 362 30 429 15 1 $T_CLICK_INSTANT
}



expd_select_slot() {
  #"""
  # Args:
  #   $1 (int): slot(1~8)
  #"""
  echo "$(date +%T) [Debug] expd_select_slot($@)..."
  zmcs 120 252 $(( 134 + $1 * 30 )) 15 1 $T_CLICK_INSTANT
}


expd_select_fleet() {
  #"""
  # Args:
  #   $1 (int): fleet_id
  #"""
  echo "$(date +%T) [Debug] expd_select_fleet($@)..."

  if [ $(chk_expd_fleet_id $1) -eq 1 ]; then
    echo "$(date +%T) [Error] Bad fleet id: $1"
    exit 1
  fi

  # Only need to click for fleet 3,4
  if (( $1 > 2 )); then
    zmcs $(( 328 + $1 * 30 )) 7 107 10 1 $T_CLICK_ANIMATE
  fi
}


expd_decide() {
  #"""
  # Decide expd_id (not sent yet)
  #"""
  echo "$(date +%T) [Info] expd_decide()..."
  zmcs 605 160 430 28 1 $T_CLICK_ANIMATE
}


expd_go() {
  #"""
  # Send out
  #"""
  echo "$(date +%T) [Info] expd_go()..."
  zmcs 535 157 431 25 1 5600
}


#"""
# compound actions
#"""


expd_switch_slot() {
  #"""
  # Switch slot to refresh click status of "next"
  #
  # Args:
  #   $1 (expd_id)
  #"""

  echo "$(date +%T) [Debug] expd_switch_slot($@) begins..."

  if (( $(expd_id_get_seaarea $1) == 1 )); then
    expd_seaarea2
  else
    expd_seaarea1
  fi
  echo "$(date +%T) [Debug] expd_switch_slot($@) done!"
}


expd_select_id() {
  #"""
  # Select expedition id and click decide button
  #
  # Args:
  #   $1 (expd_id)
  #"""
  echo "$(date +%T) [Debug] expd_select_id($@) begins..."

  local seaarea
  local slot
  # text-id specific
  local alpha
  local num
  # loop to click "next"
  local i

  # check input
  if [ $(chk_expd_id $1) -eq 1 ]; then
    echo "$(date +%T) [Error] Bad expedition id: $1"
    exit 1
  fi

  # numeric id
  if [ $(chk_num $1) -eq 0 ]; then

    seaarea=$(expd_id_get_seaarea $1)
    slot=$(( ($1 - 1) % 8 + 1 ))
    # 1) select seaarea
    eval "expd_seaarea${seaarea}"
    # 2) select slot
    expd_select_slot "$slot"

  # text id (A1~F99)
  else
    # parse id
    alpha="$(ltnum_get_lt $1)"
    num="$(ltnum_get_num $1)"

    # A??
    if [ "$alpha" == "A" ]; then
      # 1) select seaarea
      expd_seaarea1
      # 2) next until appear
      for (( i = 1; i <= num; ++i )); do
        expd_seaarea1_next
      done
      # 3) select slot
      expd_select_slot 8

    # B??
    elif [ "$alpha" == "B" ]; then
      # 1) select seaarea
      expd_seaarea2
      # 2) next until appear
      for (( i = 1; i <= num; ++i )); do
        expd_seaarea2_next
      done
      # 3) select slot
      expd_select_slot 8

    # unknown/unimplemented
    else
      echo "$(date +%T) [Error] expd_id not implemented: $1"
      exit 1
    fi
  fi

  # click "decide" (into fleet selection)
  expd_decide

  echo "$(date +%T) [Debug] expd_select_id($@) done!"
}


expd_send1() {
  #"""
  # Send one fleet at expd interface
  #
  # Args:
  #   $1 (int)
  #   $2 (expd_id)
  #"""

  echo "$(date +%T) [Debug] expd_send1($@) begins..."
  expd_select_id $2
  expd_select_fleet $1
  expd_go
  echo "$(date +%T) [Debug] expd_send1($@) done!"
}


expd_send2() {
  #"""
  # Send 2 fleets at expd interface
  #
  # Args:
  #   $1 (int): fleet_id 1
  #   $2 (expd_id): expd_id 1
  #   $3 (int): fleet_id 2
  #   $4 (expd_id): expd_id 2
  #"""

  echo "$(date +%T) [Debug] expd_send2($@) begins..."

  expd_send1 $1 $2
  # switch slot if previous expd_id is letter-numeric
  if [ $(chk_ltnum $2) -eq 0 ]; then
    expd_switch_slot $2
  fi

  expd_send1 $3 $4

  echo "$(date +%T) [Debug] expd_send2($@) done!"
}


expd_send3() {
  #"""
  # Send 3 fleets at once
  #
  # Args:
  #   $1 (expd_id): for fleet2
  #   $2 (expd_id): for fleet3
  #   $3 (expd_id): for fleet4
  #"""

  echo "$(date +%T) [Debug] expd_send3($@) begins..."

  expd_send1 2 $1
  # switch slot if previous expd_id is letter-numeric
  if [ $(chk_ltnum $1) -eq 0 ]; then
    expd_switch_slot $1
  fi

  expd_send1 3 $2
  if [ $(chk_ltnum $2) -eq 0 ]; then
    expd_switch_slot $2
  fi

  expd_send1 4 $3

  echo "$(date +%T) [Debug] expd_send3($@) done!"
}


expd_send3_6args() {
  #"""
  # Wrapper for expd_send3 with 6 args
  #
  # Args:
  #   $1:  2
  #   $2:  expd id for fleet2
  #   $3:  3
  #   $4:  expd id for fleet3
  #   $5:  4
  #   $6:  expd id for fleet4
  #"""

  echo "$(date +%T) [Debug] expd_send3_6args($@) -> expd_send3($2 $4 $6)"

  if (( $1 == 2 )) && (( $3 == 3 )) && (( $5 == 4 )); then
    expd_send3 $2 $4 $6
  else
    echo "$(date +%T) [Error] Bad Args: $@"
    exit 1
  fi
}
