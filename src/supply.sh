#!/usr/bin/env bash
#"""
# Supply actions
#"""

# dependencies
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/func.sh"


#"""
# buttons
#"""

supply_fleet() {
  #"""
  # On supply page, pick a fleet (no supply action yet)
  #
  # Args:
  #   $1 (int): 1~5  fleet number, 5="other"
  #"""
  echo "$(date +%T) [Debug] supply_fleet($@)..."
  zmcs $(( 109 + $1 * 31 )) 4 114 10  1 $T_CLICK_ANIMATE
}


supply_slot() {
  #"""
  # Hit (tick) a slot
  #
  # Args:
  #   $1 (int): 1~6  slot number
  #"""
  echo "$(date +%T) [Debug]   selecting slot $1..."
  zmcs 114 8 $(( 108 + $1 * 52 )) 6  1 $T_CLICK_ANIMATE
}


supply_all() {
  #"""
  # Hit the "smart box" left to fleet1
  #"""
  echo "$(date +%T) [Debug] supply_all()..."
  zmscs 113 8 112 8 700 1 $T_CLICK_API_ANIMATE
}


supply_both() {
  #"""
  # Hit supply_both (central) button on the bottom-right
  #"""
  echo "$(date +%T) [Debug] supply_both()..."
  zmcs 658 88 430 21 1 $T_CLICK_API_ANIMATE
}


supply_ammo() {
  #"""
  # Hit supply_ammo (left) button on the bottom-right
  #"""
  echo "$(date +%T) [Debug] supply_ammo()..."
  zmcs 758 21 428 24 1 $T_CLICK_API_ANIMATE
}


supply_fuel() {
  #"""
  # Hit supply_fuel (right) button on the bottom-right
  #"""
  echo "$(date +%T) [Debug] supply_fuel()..."
  zmcs 625 21 429 22 1 $T_CLICK_API_ANIMATE
}


#"""
# Compound Actions
#"""


supply_generic() {
  #"""
  # Generic supply action for slots (after fleet choice)
  #
  # Args:
  #   $1      both/ammo/fuel
  #   $2..$7  slots (indefinite number)
  #"""
  echo "$(date +%T) [Debug] supply_generic($@) begins..."

  # check parameter count
  if [ $# -lt 2 ]; then
    echo "$(date +%T) [Error] Bad parameters: $@"
    exit 1
  fi

  # click slots
  local it
  for it in "${@:2}"; do
    supply_slot $it
  done

  # hit supply
  if [ "$1" == "both" ] || [ "$1" == "b" ]; then
    supply_both
  elif [ "$1" == "ammo" ] || [ "$1" == "a" ]; then
    supply_ammo
  elif [ "$1" == "fuel" ] || [ "$1" == "f" ]; then
    supply_fuel
  else
    echo "$(date +%T) [Error] Bad resource: $1"
    exit 1
  fi

  echo "$(date +%T) [Debug] supply_generic($@) done!"
}


supply() {
  #"""
  # Alias of 'supply_generic both '
  # Args:
  #   $1...  1~6  slots to be supplied
  #"""
  supply_generic "both" $@
}


supply12() {
  #"""
  # Hit supply button 12 times for the stupid daily quest
  #"""
  echo "$(date +%T) [Debug] supply12() begins..."
  local i=1
  while (( i <= 6 )); do
    echo "$(date +%T) [Debug]   slot $i"
    supply_generic "fuel" $i
    supply_generic "both" $i
    (( i += 1 ))
  done
  echo "$(date +%T) [Debug] supply12() done!"
}


supply_all_expd_fleets() {
  #"""
  # Supply fleet 2~4. Fleet 1 is untouched.
  #"""
  echo "$(date +%T) [Debug] supply_all_expd_fleets() begins..."
  local i
  for i in {2..4}; do
    supply_fleet $i
    supply_all
  done
  echo "$(date +%T) [Debug] supply_all_expd_fleets() done!"
}
