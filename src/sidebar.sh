#!/usr/bin/env bash
#"""
# Sidebar (left panel) actions along with:
#
#     * top-left big button
#     * Improvement Factory (Ms. Akaishi)
#"""

# dependencies
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/func.sh"


sidebar_room() {
  #"""
  # Back to Teitoku Room
  #"""
  echo "$(date +%T) [Debug] sidebar_room()..."
  zmcs 65 20 225 60  1 $T_CLICK_API_LONG
}


sidebar_hensei() {
  echo "$(date +%T) [Debug] sidebar_hensei()..."
  zmcs 10 20 135 30  1 $T_CLICK_API
}


sidebar_supply() {
  echo "$(date +%T) [Debug] sidebar_supply()..."
  zmcs 10 20 190 30  1 $T_CLICK_API
}


sidebar_refit() {
  echo "$(date +%T) [Debug] sidebar_refit()..."
  zmcs 10 20 144 30  1 $T_CLICK_API
}


sidebar_docking() {
  echo "$(date +%T) [Debug] sidebar_docking()..."
  zmcs 10 20 297 30  1 $T_CLICK_API_ANIMATE
}


sidebar_factory() {
  echo "$(date +%T) [Debug] sidebar_factory()..."
  zmcs 10 20 350 30  1 $T_CLICK_API
}


topleft() {
  #"""
  # The big top-left button
  #"""
  echo "$(date +%T) [Debug] topleft()..."
  zmcs 5 75 8 67  1 $T_CLICK_API_LONG
}


sidebar_improvement() {
  #"""
  # Enter Akaishi Factory
  #"""
  echo "$(date +%T) [Debug] sidebar_improvement()..."
  zmcs 13 68 414 46  1 $T_CLICK_API_ANIMATE
}
