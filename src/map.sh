#!/usr/bin/env bash
#"""
# Map actions.
#
# Everything after "sortie" & before "formation choice" is coded here.
#"""

# project settings
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/func.sh"


#"""
# compass
#"""

map_sleep_no_compass() {
  echo "$(date +%T) [Info] map_sleep_no_compass() begins..."
  sleep $(( T_MAP_NO_COMPASS / 1000 ))
  echo "$(date +%T) [Info] map_sleep_no_compass() done!"
}


map_compass() {
  #"""
  # Hit compass
  #"""
  echo "$(date +%T) [Debug] map_compass() begins..."
  click_safe $T_MAP_COMPASS
  echo "$(date +%T) [Debug] map_compass() done!"
}



map_compass_detection() {
  #"""
  # Hit compass and wait for detection animation
  #"""
  echo "$(date +%T) [Info] map_compass_detection() begins..."
  click_safe $(( T_MAP_COMPASS + T_MAP_DETECTION ))
  echo "$(date +%T) [Info] map_compass_detection() done!"
}


#"""
# land-based aircraft
#"""
map_lbas_confirm(){
  #"""
  # confirm lbas selection
  #"""
  echo "$(date +%T) [Info] map_lbas_confirm() begins..."
  zmcs 18 168 49 18 1 $T_CLICK_API_ANIMATE
  echo "$(date +%T) [Info] map_lbas_confirm() done!"
}
