#!/usr/bin/env bash
#"""
# Common Functions
#
# Intended to be sourced by all action scripts.
# Includes project and window config, game constants and variable checkers.
#"""

# project settings
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"

# window config
source "${CONFHOME}/X.conf"

# variable checker
source "${SRCHOME}/chkvar.sh"

# constants
source "${SRCHOME}/const.sh"

#"""
# Time Functions
# ===============================
#"""


randsleepms() {
  #"""
  # 3-level random sleep function
  #
  # Args:
  #   $1  base sleep time (ms)
  #
  # Returns:
  #     int: randomized sleep in ms
  #
  #     The distribution:
  #
  #     $ret = $1 * (1 +
  #                  20% * U01 * P_A +   # 110/128 odds within [100%, 120%)
  #                  30% * U01 * P_B +   #  16/128 odds within [120%, 150%)
  #                  50% * U01 * P_C)    #   2/128 odds within [150%, 200%)
  #
  #     where U01 ~ uniform(0,1),
  #           Dxx ~ xx% chance = 1, otherwise 0
  #"""

  local rnd=$((RANDOM % 12800))

  if (( rnd < 11000 )); then
    echo $(( $1 * ( 100 + rnd % 20) / 100 ))
  elif (( rnd < 12600 )); then
    echo $(( $1 * ( 120 + rnd % 30) / 100 ))
  else
    echo $(( $1 * ( 150 + rnd % 50) / 100 ))
  fi
}


ms() {
  #"""
  # Move-sleep (no click) action unit.
  #
  # NOTE: zoom in ``zms`` before passing into this
  #
  # Args:
  #     $1  xbegin   between [xbegin, xbegin+xrng]
  #     $2  xrng
  #     $3  ybegin
  #     $4  yrng
  #     $5  tmin     in milliseconds
  #
  # Global Variable: $DBG_RNDTIME  system randomization
  #"""

  # move
  xdotool mousemove $((XORIG + $1 + RANDOM % $2)) $((YORIG + $3 + RANDOM % $4))

  # sleep milliseconds plus some us
  local t=$(( RANDOM % 1000 + T_HOLD * 1000 ))
  if [ "$DBG_RNDTIME" == "n" ]; then
    (( t += 1000 * $5 ))
  else
    (( t += 1000 * $(randsleepms $5) ))
  fi
  ${BINHOME}/usleep.exe $t
}


zms() {
  #"""
  # Zoom wrapper before passing into ms(). See ms() for args.
  #"""
  local new1=$(( $1 * ZOOM / 100 ))
  local new2=$(( $2 * ZOOM / 100 ))
  local new3=$(( $3 * ZOOM / 100 ))
  local new4=$(( $4 * ZOOM / 100 ))
  ms $new1 $new2 $new3 $new4 $5
}



mcs() {
  #"""
  # move-click-sleep action unit
  #
  # NOTE: zoom in ``zmcs`` before passing into this
  #
  # Args:
  #     $1  xbegin   between [xbegin, xbegin+xrng]
  #     $2  xrng
  #     $3  ybegin
  #     $4  yrng
  #     $5  click    1 = left button
  #     $6  tmin     in milliseconds
  #
  # Global Variable: $DBG_RNDTIME  system randomization
  #"""

  # move and sleep for a little while
  xdotool mousemove $((XORIG + $1 + RANDOM % $2)) $((YORIG + $3 + RANDOM % $4))
  ${BINHOME}/usleep.exe $T_BEFORE_CLICK

  # click
  xdotool click $5

  # sleep ms plus some us
  local t=$(( RANDOM % 1000 + T_HOLD * 1000 ))
  if [ "$DBG_RNDTIME" == "n" ]; then
    (( t += 1000 * $6 ))
  else
    (( t += 1000 * $(randsleepms $6) ))
  fi
  ${BINHOME}/usleep.exe $t
}


zmcs() {
  #"""
  # Zoom wrapper before passing into mcs(). See mcs() for args.
  #"""
  local new1=$(( $1 * ZOOM / 100 ))
  local new2=$(( $2 * ZOOM / 100 ))
  local new3=$(( $3 * ZOOM / 100 ))
  local new4=$(( $4 * ZOOM / 100 ))
  mcs $new1 $new2 $new3 $new4 $5 $6
}


mscs() {
  #"""
  # Move-sleep-click-sleep Action Unit
  #
  # NOTE: zoom in ``zmscs`` before passing into this
  #
  # Args:
  #     $1  xbegin   between [xbegin, xbegin+xrng]
  #     $2  xrng
  #     $3  ybegin
  #     $4  yrng
  #     $5  t        in ms, NO RANDOM
  #     $6  click    1 = left button
  #     $7  tmin     in milliseconds
  #
  # Global Variable: $DBG_RNDTIME  system randomization
  #"""

  # move
  xdotool mousemove $((XORIG + $1 + RANDOM % $2)) $((YORIG + $3 + RANDOM % $4))
  # sleep
  ${BINHOME}/usleep.exe $(( 1000 * $5 ))
  # click
  xdotool click $6
  # sleep ms plus some us
  local t=$(( RANDOM % 1000 + T_HOLD * 1000 ))
  if [ "$DBG_RNDTIME" == "n" ]; then
    (( t += 1000 * $7 ))
  else
    (( t += 1000 * $(randsleepms $7) ))
  fi
  ${BINHOME}/usleep.exe $t
}


zmscs() {
  #"""
  # Zoom wrapper before passing into ``mscs()``. See ``mscs()`` for args.
  #"""
  local new1=$(( $1 * ZOOM / 100 ))
  local new2=$(( $2 * ZOOM / 100 ))
  local new3=$(( $3 * ZOOM / 100 ))
  local new4=$(( $4 * ZOOM / 100 ))
  mscs $new1 $new2 $new3 $new4 $5 $6 $7
}


countdown() {
  #"""
  # Args:
  #  $1 (int): sleep time (seconds)
  #"""
  echo -n "$(date +%T) [Info] counting down..."
  local i=$1
  while (( i > 0 )); do
    echo -n "$i..."
    sleep 1
    (( i -= 1 ))
  done
  echo "0!"
}


#"""
# Generic Actions
# ======================
#"""


click_anywhere() {
  #"""
  # Generic arbitrary click
  #
  # Args:
  #   $1 (int): sleep time in milliseconds
  #"""
  echo "$(date +%T) [Debug] click_anywhere($@)..."
  zmcs 140 530 125 280 1 $1
}


click_safe() {
  #"""
  # Generic safe-click when you are not sure about you are in combat,
  # expedition or Teitoku room
  #
  # Args:
  #   $1 (int): sleep time in milliseconds
  #"""
  echo "$(date +%T) [Debug] click_safe($@)..."
  zmcs 570  50 430  25 1 $1
}



read_safe() {
  #"""
  # Discard unwanted characters before read
  #
  # Args:
  #   $1 (str): variable name to be read
  #   $2 (str): prompt message
  #"""

  # check number of arguments
  if (( $# != 2 )); then
    echo "$(date +%T) [Error] Wrong argument count: $#"
    exit 1
  fi

  # discarding unwanted characters...."
  local discarded
  read -N 1000 -t 0.01 discarded

  # read non-local variable
  read -p "***USER INPUT*** $2:" "$1"
}


#"""
# Text Parsers
# ======================
#"""

ltnum_get_lt() {
  #"""
  # Get letter from "letter-num" text
  #
  # Args:
  #   $1 (str): any text
  #
  # Returns:
  #   str: first letter of "letter-num" text / empty (other)
  #"""

  if [ $(chk_ltnum $1) -eq 0 ]; then
    echo "${1:0:1}"
  else
    echo ""
  fi
}


ltnum_get_num() {
  #"""
  # Get numeric part from "letter-num" text
  #
  # Args:
  #   $1 (str): any text
  #
  # Returns:
  #   int: number part (no leading 0) of letter-num text / empty otherwise
  #"""

  if [ $(chk_ltnum $1) -eq 0 ]; then
    local a="${1:1:3}"
    echo $(( 10#$a ))
  else
    echo ""
  fi
}
