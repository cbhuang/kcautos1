#!/usr/bin/env bash
#"""
# SPE (Sortie-Practice-Expedition) actions
#"""

# dependencies
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/func.sh"


spe_sortie() {
  echo "$(date +%T) [Debug] spe_sortie()..."
  zmcs 161 128 169 106 1 $T_CLICK_API_ANIMATE
}


spe_practice() {
  echo "$(date +%T) [Debug] spe_practice()..."
  zmcs 392 118 162 112 1 $T_CLICK_API_ANIMATE
}


spe_expd() {
  echo "$(date +%T) [Debug] spe_expd()..."
  zmcs 617 121 159 118 1 $T_CLICK_API_ANIMATE
}
