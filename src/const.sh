#!/usr/bin/env bash
#"""
# Global Game Constants
#"""


#"""
# Generic Wait Time of Clicks
# =================================
#"""

T_BEFORE_CLICK=428571
#"""mcs() sleep time beteen move and click in microseconds"""

T_CLICK_INSTANT=812
#"""Sleep for instant reactive clicks (no API request or animation)' in ms"""

T_CLICK_ANIMATE=1487
#"""Sleep for animative clicks (no API request) in ms"""

T_CLICK_API=3522
#"""Sleep for API request clicks, (no animation) in ms"""

T_CLICK_API_ANIMATE=4366
#"""Sleep for API request clicks and animate' in ms"""

T_CLICK_API_LONG=6070
#"""
# Sleep for "potentially longer" API request clicks in ms.
# (From experience, actions to room can sometimes lag)
#"""

T_HOLD=0
#"""
# Unconditional extra delay after click. For debug only!
#"""


#"""
# Combat
# =================================
#"""

# switch of formation 6
FORMATION6_EXIST="n"

# pre-shelling phase
T_COMBAT_DETECTION=13000    # from beginning to detection
T_COMBAT_JETASSULT=9000     # (to be tested)
T_COMBAT_AERIAL=9000        # aerial combat
T_COMBAT_SUPPORT=6000       # (to be tested)
T_COMBAT_ANTISS=2000        # (to be tested)
T_COMBAT_TORPEDO_OPEN=4000  # opening torpedo
T_COMBAT_DIRECTION=3000     # show direction

# day combat
T_COMBAT_MIDDMG=5000           # medium damage animation
T_COMBAT_SHELL_ORD=1800        # ordinary shelling
T_COMBAT_SHELL_CV_ORD=3000     # ordinary CV shelling
T_COMBAT_SHELL_DOUBLE=2500     # double attack (with seaplanes)
T_COMBAT_SHELL_SPOT=4000       # artillery spotting (with seaplanes)
T_COMBAT_SHELL_CV_CI=7000      # 2017.9 New CV CI attack
T_COMBAT_TORPEDO_CLOSE=7000    # closing torpedo to yasen choice
T_COMBAT_2NDRND=25000          # empirical harsh 2nd round shelling correction

# night combat
T_COMBAT_NIGHT_OPEN=7000
T_COMBAT_NIGHT_STARSHELL=10000    # zhao ming dan
T_COMBAT_NIGHT_CI=6000            # 5-3 Ri class flagship
T_COMBAT_NIGHT_SHELL_DOUBLE=2500  # currently same as day battle
T_COMBAT_NIGHT_CLOSE=3000         # yasen close to counting exp

# post-combat report
T_COMBAT_END_RANK=11000         # battle end to rank confirm
T_COMBAT_RANK_EXP=5000          # rank confirm to exp confirm
T_COMBAT_EXP_DROP=8000          # EXP(or rank) to drop
T_COMBAT_DROP_ADV=$T_CLICK_API     # drop confirm(or exp) to advance decision
T_COMBAT_DROP_ROOM=$T_CLICK_API_LONG    # drop confirm to admiral room

# empirical time constants
# 2 rounds
T_COMBAT_DAY=$(( T_COMBAT_DETECTION + \
                 T_COMBAT_DIRECTION + \
                 T_COMBAT_SHELL_ORD * 9 + \
                 T_COMBAT_SHELL_SPOT * 1 + \
                 T_COMBAT_SHELL_CV_ORD * 3 + \
                 T_COMBAT_SHELL_CV_CI * 2 + \
                 T_COMBAT_TORPEDO_CLOSE ))
T_COMBAT_NIGHT=$(( T_COMBAT_NIGHT_OPEN + \
                   T_COMBAT_NIGHT_STARSHELL + \
                   T_COMBAT_SHELL_DOUBLE * 8 + \
                   T_COMBAT_NIGHT_CLOSE + \
                   T_COMBAT_MIDDMG ))
T_COMBAT_NIGHT_6v6=$(( T_COMBAT_NIGHT_OPEN + \
                       T_COMBAT_NIGHT_STARSHELL + \
                       T_COMBAT_NIGHT_SHELL_DOUBLE * 9 + \
                       T_COMBAT_NIGHT_CI * 3 + \
                       T_COMBAT_NIGHT_CLOSE + \
                       T_COMBAT_MIDDMG * 2 ))
# Practice: assume 5DD1MRY vs 2BB4CV
T_COMBAT_DAY_WORST=$(( T_COMBAT_DETECTION + \
                       T_COMBAT_AERIAL + \
                       T_COMBAT_TORPEDO_OPEN + \
                       T_COMBAT_DIRECTION + \
                       T_COMBAT_SHELL_ORD * 10 + \
                       T_COMBAT_SHELL_SPOT * 4 + \
                       T_COMBAT_SHELL_CV_ORD * 2 + \
                       T_COMBAT_SHELL_CV_CI * 6 + \
                       T_COMBAT_TORPEDO_CLOSE + \
                       T_COMBAT_MIDDMG * 5 ))


#"""
# Map
# =================================
#"""

# Regular compass hit
T_MAP_COMPASS=11000
# T_MAP_COMPASS is also applicable if next node is maelstrom (tested)
T_MAP_COMPASS_MAELSTROM=$T_MAP_COMPASS
# wait time when no compass hit is required
T_MAP_NO_COMPASS=9000
T_MAP_NO_COMPASS_MAELSTROM=$T_MAP_NO_COMPASS
# animated detection (no items found like W6-3)
T_MAP_DETECTION=9000


#"""
# Expedition
# =================================
#"""

# duration (seconds): expd 01~40
T_EXPD=( 0
     900   1800   1200   3000   5400   2400   3600  10800
   14400   5400  18000  28800  14400  21600  43200  54000
    2700  18000  21600   7200   8400  10800  14400  30000
  144000 288000  72000  90000  86400 172800   7200  86400
     900   1800  25200  32400   9900  10500 108000  24600
)

# duration: expd A1~A3
T_EXPD_A=( 0
    1500   3300   8100
)

# duration: expd B1~B2
T_EXPD_B=( 0
    2100  31200
)

#"""
# Expd Time Parameters Explained
# ---------------------------------
#
#             min        max     nominal
# warn       recv  recv  recv    return      send
# ---|----------|---?----|---------|-----------?-----> t
#     <- WARN ->
#                <---- EARLY ----->
#                <-RND->
#                   ?<-- RESEND (estimated) -->?
#"""

# Warn at seconds becore receive
T_EXPD_WARN=80

# Seconds to receive earlier
T_EXPD_EARLY=57

# Randomized receive region (can choose to not use)
T_EXPD_RND=14

# seconds estimated to receive and resend 1 fleet
T_EXPD_RESEND=70
