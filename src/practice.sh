#!/usr/bin/env bash
#"""
# Practice actions
#"""

# dependencies
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/func.sh"

#"""
# top bar
#"""

practice_sortie() {
  echo "$(date +%T) [Debug] practice_sortie()..."
  zmcs 549 61 117 14  1 $T_CLICK_API_ANIMATE
}

practice_expd() {
  echo "$(date +%T) [Debug] practice_expd()..."
  zmcs 720 62 119 12  1 $T_CLICK_API_ANIMATE
}

#"""
# Rivals
#"""

practice_rival() {
  #"""
  # Pick a rival
  #
  # Args:
  #   $1 (int): top=1,...,5=bottom
  #"""
  echo "$(date +%T) [Debug] practice_rival($@)..."
  zmcs 222 336 $(( 182 + ($1 - 1) * 56 )) 30 1 $T_CLICK_API_ANIMATE
}


practice_rival_confirm() {
  echo "$(date +%T) [Debug] practice_rival_confirm($@)..."
  zmcs 182 110 396 22 1 $T_CLICK_ANIMATE
}


practice_go() {
  echo "$(date +%T) [Debug] practice_go($@)..."
  zmcs 380 148 411 30 1 $T_CLICK_API_ANIMATE
}
