#!/usr/bin/env bash
#"""
# Question Functions
#"""

# dependencies
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/func.sh"


question_ask() {
  #"""
  # Ask an array of questions
  #
  # Args:
  #   $1 (str):  qs[@]
  #
  # NOTE:
  #     For bash >= 4.3, an array can be pass by name:
  #
  #     ``local -n _qs="$1"``
  #
  #     For bash <= 4.2, the questions must be passed by value and
  #     expanded by ! (indirect expansion).
  #     Pass by value is OK since a command can take >= 1M characters
  #     in modern 64-bit PCs.
  #"""

  # indirect expansion of array values
  local -a _qs=("${!1}")

  local res
  local q

  # Give a chance to skip all questions
  read -p "Check safety questions? (y/n)" res
  if [ $(chk_no $res) -eq 0 ]; then
    # skip
    echo "$(date +%T) [Info] Safety questions were skipped!"
  else
    # ask
    echo "$(date +%T) [Info] Checking safety questions..."
    echo "**If anything didn't met, go adjust without quitting!**"
    # no indirect expansion here
    for q in "${_qs[@]}"; do
      read -p "  $q (n/y): " res
      if [ $(chk_yes $res) -eq 1 ]; then
        echo "$(date +%T) [Info] User abort!"
        exit 1
      fi
    done
  fi
}
