#!/usr/bin/env bash
#"""
# Aggregation library of all action functions.
# Compound actions are also implemented here.
#
# For files in ``${PRJHOME}/scripts`` to use:
#
# .. code-block:: bash
#
#    # 1. Source project settings
#    DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#    source "${DIR_THIS}/../conf/prj.conf"
#
#    # 2. Source the main library, once for all
#    source "${SRCHOME}/action.sh"
#"""

# dependencies
# NOTE: ". ./func.sh" fails when this file is being sourced.
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# const.sh, chkvar.sh are sourced indirectly
source "${DIR_THIS}/func.sh"

# action modules
source "${SRCHOME}/room.sh"
source "${SRCHOME}/sidebar.sh"
source "${SRCHOME}/quest.sh"
source "${SRCHOME}/hensei.sh"
source "${SRCHOME}/factory.sh"
source "${SRCHOME}/supply.sh"
source "${SRCHOME}/spe.sh"
source "${SRCHOME}/sortie.sh"
source "${SRCHOME}/practice.sh"
source "${SRCHOME}/expedition.sh"
source "${SRCHOME}/map.sh"
source "${SRCHOME}/combat.sh"
source "${SRCHOME}/question.sh"


########################################
# room/sidebar-based compound actions
########################################

sidebar_to_world() {
  #"""
  # Send fleet1 to world $1-$2
  #
  # Args:
  #   $1 (int): seaarea
  #   $2 (int): map
  #"""
  echo "$(date +%T) [Info] sidebar_to_world($@) begins..."

  if [[ ! $(chk_world $1 $2) -eq 0 ]]; then
    echo "$(date +%T) [Error] Bad World: $1-$2"
  fi

  sidebar_room
  room_spe
  spe_sortie
  sortie_world $1 $2
  #sortie_fleet 1
  sortie_go
  echo "$(date +%T) [Info] sidebar_to_world($@) done!"
}


sidebar_to_practice() {
  #"""
  # Args:
  #   $1  rival slot
  #   $2  formation
  #"""
  echo "$(date +%T) [Info] sidebar_to_practice($@) begins..."

  # check
  if [ $(chk_practice_rival $1) -eq 1 ] || \
     [ $(chk_formation_single $2) -eq 1 ]; then
    echo "$(date +%T) [Error] Bad parameters: $@"
    exit 1
  fi

  # supply
  sidebar_supply
  supply_all
  # pick rival
  sidebar_room
  room_spe
  spe_practice
  practice_rival $1
  practice_rival_confirm
  # go
  practice_go
  combat_formation $2

  echo "$(date +%T) [Info] sidebar_to_practice($@) done!"
}


sidebar_expd() {
  #"""
  # sidebar to expedition interface
  #"""
  echo "$(date +%T) [Debug] sidebar_to_expd($@)..."
  sidebar_room
  room_spe
  spe_expd
}


room_disintegrate_top() {
  #"""
  # room -> disintegrate_top N
  #
  # Args:
  #   $1  >=1  disintegrate_top N
  #"""
  echo "$(date +%T) [Debug] room_disintegrate_top($@)..."
  room_factory
  factory_disintegrate
  disintegrate_top $1
}


room_quest_daily_item() {
  #"""
  # room -> daily quest item N
  #
  # Args:
  #   $1  1~5  daily quest item id
  #"""
  echo "$(date +%T) [Debug] room_quest_daily_item($@)..."
  room_quest
  quest_daily
  quest_item $1
}


######################
# expedition
######################

expd_cycle_sll_N_short() {
  #"""
  # calculate # of short cycles in expd_cycle_sll
  #
  # Args:
  #   $1  seconds  expd time fleet2
  #   $2  seconds  expd time fleet3
  #   $3  seconds  expd time fleet4
  #
  # Returns:
  #    # of short cycles (exclude the last one which synchronizes with other fleets)
  #"""

  local t_min=$(( $2<$3?$2:$3 ))
  local t_max=$(( $2<$3?$3:$2 ))

  # Can tolerate ($1 / 3) delay for last short_expd return
  local t_max_revised=$(( t_max - $1 * 2 / 3 ))
  local t_short_last_max=$(( t_min<t_max_revised?t_min:t_max_revised ))

  # time to 1st long expd return
  local t_num=$(( t_short_last_max - 60 ))
  # time consumed (max. randomized) per short cycle
  local t_den=$(( $1 - T_EXPD_EARLY + T_EXPD_RND + T_EXPD_RESEND ))

  echo $(( t_num / t_den ))
}


expd_receive() {
  #"""
  # Args:
  #   $1  y/n: already in admiral room
  #   $2  1~3: how many fleets are coming back
  #"""
  echo "$(date +%T) [Info] expd_receive($@) begins..."

  # process args
  if [ $(chk_yes $1) -eq 0 ]; then
    local inroom=y
  elif [ $(chk_no $1) -eq 0 ]; then
    local inroom=n
  else
    echo "$(date +%T) [Error] (Critical) Are you in admiral room?: $1"
    exit 1
  fi

  local n_flt
  if [ -z "$2" ]; then
    echo "$(date +%T) [Debug] No #returning fleets given, set to 1."
    n_flt=1
  elif (( $2 >= 1 )) && (( $2 <= 3 )); then
    echo "$(date +%T) [Debug] #Returning fleets = $2"
    n_flt=$2
  else
    echo "$(date +%T) [Error] Bad #returning fleets: $2"
    exit 1
  fi

  # trigger return
  if [ "$inroom" == "y" ]; then
    room_hensei
  fi
  sidebar_room

  # receive all fleets
  local i=1
  while (( i <= n_flt )); do
    echo "$(date +%T) [Debug]  receive count=$i"
    # click_safe to prevent interruption
    click_safe 12300
    click_safe $T_CLICK_INSTANT
    click_safe $T_CLICK_INSTANT
    (( i += 1 ))
  done

  echo "$(date +%T) [Info] expd_receive($@) done!"
}


expd_resend(){
  #"""
  # Args:
  #   $1  y/n      in admiral room or not
  #   $2  expd_id
  #   $3  2~4      which fleet
  #"""
  echo "$(date +%T) [Info] expd_resend($@) begins..."

  # capture arguments
  if [ $(chk_yes $1) -eq 0 ]; then
    local inroom=y
  elif [ $(chk_no $1) -eq 0 ]; then
    local inroom=n
  else
    echo "$(date +%T) [Error] (Critical) Are you in admiral room?: $1"
    exit 1
  fi

  if [ $(chk_expd_id $2) -eq 0 ]; then
    local expd_id=$2
  else
    echo "$(date +%T) [Error] Bad expd id: $2"
    exit 1
  fi

  if [ $(chk_expd_fleet_id $3) -eq 0 ]; then
    local flt=$3
  else
    echo "$(date +%T) [Debug] No fleet given. Set to 2."
    local flt=2
  fi

  # supply
  if [ "$inroom" == "y" ]; then
    room_supply
  else
    sidebar_supply
    # NOTE: Every other places have sidebar access, except quest page
  fi
  supply_fleet $flt
  supply_all

  # go
  sidebar_expd
  expd_send1 $flt $expd_id
  echo "$(date +%T) [Info] expd_resend($@) done!"
}


# simple cycle for single fleet
expd_cycle_single_fleet() {
  #"""
  # Args:
  #   $1  2~4      fleet
  #   $2  expd_id
  #   $3  >=1      #cycles
  #"""
  echo "$(date +%T) [Info] expd_cycle_single_fleet($@) begins..."

  # check parameters
  if [ $(chk_expd_fleet_id $1) -eq 1 ]; then
    echo "$(date +%T) [Error] Bad expd fleet id: $1"
    exit 1
  fi

  if [ $(chk_expd_id $2) -eq 1 ]; then
    echo "$(date +%T) [Error] Bad expd id: $2"
    exit 1
  fi

  if [ -z "$3" ]; then
    echo "$(date +%T) [Warning] #Cycles missing, set to 1"
    local N=1
  else
    local N=$3
  fi

  # main
  local i=1
  local T="$(expd_id_get_time $2)"

  while (( i <= N )); do

    echo "$(date +%T) [Info] Expd cycle $i of $N begins..."

    # 1. supply
    sidebar_supply
    supply_fleet $1
    supply_all

    # 2. send
    sidebar_expd
    expd_send1 $1 $2

    # 3. wait
    echo "$(date +%T) [Info] Sleep $T seconds..."
    # sleep the majority of time
    sleep $(( T - T_EXPD_EARLY - T_EXPD_WARN ))
    # warn
    expd_sleep_warn_sidebar $(( RANDOM % T_EXPD_RND + T_EXPD_WARN ))

    # 4. receive
    expd_receive n 1
    # back to sidebar
    room_supply

    echo "$(date +%T) [Info] Expd cycle $i of $N done!"
    (( i += 1 ))
  done

  echo "$(date +%T) [Info] expd_cycle_single_fleet($@) done!"
}


# resend fleet2 within a cycle
expd_cycle_resend_fleet2() {
  #"""
  # Args:
  #   $1  expd_id  practically: 2, 3, 6, 21, A1
  #   $2  >=1      times to repeat
  #"""

  echo "$(date +%T) [Info] expd_cycle_resend_fleet2($@) begins...."

  local i=1
  local T="$(expd_id_get_time $1)"
  while (( i <= $2 )); do

    echo "$(date +%T) [Info] Waiting for expd $1 ($i of $2)"

    # sleep until 1min to fleet return
    sleep $(( T - T_EXPD_EARLY - T_EXPD_WARN ))
    # warn
    expd_sleep_warn_sidebar $(( RANDOM % T_EXPD_RND + T_EXPD_WARN ))

    expd_receive n 1
    expd_resend y $1 2

    (( i += 1 ))
  done

  echo "$(date +%T) [Info] expd_cycle_resend_fleet2($@) done!"
}


# expd cycle: short-long-long pattern
expd_cycle_sll() {
  #"""
  # Args:
  #   $1~$3  expd_id  expd id for fleet 2~4
  #   $4     1~99     cycles to do
  #"""

  echo "$(date +%T) [Info] expd_cycle_sll($@) begins..."

  # check parameters
  if [ $(chk_expd_id $1) -eq 1 ] || [ $(chk_expd_id $2) -eq 1 ] || \
     [ $(chk_expd_id $3) -eq 1 ]; then
    echo "$(date +%T) [Error] Bad expd id: $1 $2 $3"
    exit 1
  fi

  # check T_EXPD[$id]
  local ts="$(expd_id_get_time $1)"
  local tl1="$(expd_id_get_time $2)"
  local tl2="$(expd_id_get_time $3)"
  if (( (ts / 900) * (tl1 / 900) * (tl2 / 900) == 0 )); then
    echo "$(date +%T) [Error] Bad T_EXPD[], please check const.sh !"
    exit 1
  fi

  # check N
  local re='^[0-9]{1,2}$'
  if [[ $4 =~ $re ]]; then
    local N=$4
  else
    echo "$(date +%T) [Debug] Bad #cycles: $4, set to 1"
    local N=1
  fi

  # Calculate

  # Number of short expds per cycle. The last synchronous one is excluded.
  local N_short=$(expd_cycle_sll_N_short $ts $tl1 $tl2)

  echo "$(date +%T) [Debug] Expd $1 will be resent $N_short times within a cycle!"

  ## main cycle
  local i=1
  local j
  while (( i <= N )); do

    echo "$(date +%T) [Info] Expedition cycle $i of $N begins..."

    # 1. supply
    sidebar_supply
    supply_all_expd_fleets

    # 2. send all
    sidebar_expd
    expd_send3 $1 $2 $3

    # 3. Resend expedition $1 for $N_short times
    local t_i=$(date +%s)
    expd_cycle_resend_fleet2 $1 $N_short
    local t_consumed=$(( $(date +%s) - t_i ))

    # 4. receive all fleets
    local t_max=$(( tl1>tl2?tl1:tl2 ))
    local t_left=$(( t_max - t_consumed ))
    # wait for max(t_left, T_EXPD[$1]). NOTE: wait for latest fleet, which is uncertain!
    local t_last=$(( t_left>ts?t_left:ts ))
    echo "$(date +%T) [Info]   Waiting for all, $t_last seconds left..."

    # sleep majority of time
    sleep $(( t_last - T_EXPD_EARLY - T_EXPD_WARN ))
    # warn
    expd_sleep_warn_sidebar $(( RANDOM % T_EXPD_RND + T_EXPD_WARN ))

    expd_receive n 3
    # back to sidebar
    room_supply
    echo "$(date +%T) [Info] Expidition cycle $i of $N done!"

    (( i += 1 ))
  done

  echo "$(date +%T) [Info] expd_cycle_sll($@) done!"
}


expd_generic() {
  #"""
  # The ultimate expedition time scheduling program
  #
  # Args:
  #   $1 (expd_id): fleet 2
  #   $2 (expd_id): fleet 3
  #   $3 (expd_id): fleet 4
  #   $4 (int): duration (minutes)
  #   $5 (char): edit $SHIPFILE
  #
  # --------  ---------------  ------------------------
  # #RetFlts  Time to Resend   Set Delay
  # --------  ---------------  ------------------------
  #        1  39s              None (<60s)
  #        2  68s (+29s)       All later schedule +20s
  #        3  98s (+30s)       All later schedule +50s
  # --------  ---------------  ------------------------
  #"""

  echo "$(date +%T) [Info] expd_generic($@) begins..."

  # expedition id and time (index by fleet_id)
  local EXID=( 0 0 $1 $2 $3 )
  local T_EXID=( 0 0
    $(expd_id_get_time $1)
    $(expd_id_get_time $2)
    $(expd_id_get_time $3)
  )

  local DURATION=$(( $4 * 60 ))

  # Array of returning time
  local T_RET=()
  # #elems of T_RET
  local N_RET
  # current index (0=first sent)
  local idx_ret=0

  # Array of "list of returning fleets (space-separated)"
  local LS_FLT=()
  # current list of returning fleets
  local ls_flt_now

  # Array of returning fleet count
  local RET_COUNT=()
  # current number of fleets (= #parameters )
  local n_flt

  # Temp associative array of returning fleets (space separated)
  # The Array is automatically local.
  # e.g. tmp_ls_flt["3000"] = returning fleets at 3000s (may be 2 3)
  declare -A tmp_ls_flt

  # Time of first send
  local Ti

  # DBG_RNDTIME original value
  local DBG_RNDTIME_orig=$DBG_RNDTIME

  # shiplist
  local tmp_ships=""
  local ships=()
  local n_ships
  local idx_ship=0
  local ship_now

  # other temp vars
  local i
  local j
  local tmp_time
  # space-separated string of returning times
  local tmp_timestr=""
  local flt1
  local flt2

  #"""
  # Schedule
  #"""

  # Calculate Return Schedule
  # for fleet 2,3,4 x each return time
  for i in {2..4}; do
    for (( j = 1; j <= DURATION / T_EXID[i]; ++j )); do

      # time of return
      tmp_time=$(( j * T_EXID[i] ))

      # record time
      tmp_timestr+="$tmp_time "

      # record returning fleet(s) of the time
      tmp_ls_flt["$tmp_time"]+="${i} "

    done
  done

  # Make returning time unique and numerically sorted
  while read tmp_time; do
    T_RET+=("$tmp_time")
  done < <( echo $tmp_timestr | xargs -n1 | sort -nu )

  N_RET=${#T_RET[@]}

  # Set LS_FLT and returning fleet count
  for (( i = 0; i < N_RET; ++i )); do

    tmp_time=${T_RET[i]}
    ls_flt_now="${tmp_ls_flt["$tmp_time"]}"
    # append list of returning fleets
    LS_FLT+=( "$ls_flt_now" )
    RET_COUNT+=( $(echo $ls_flt_now | wc -w ) )

  done

  # delay all subsequent actions if >1 fleets returned
  for (( i = 0; i < N_RET; ++i )); do

    # 2 fleets
    if (( ${RET_COUNT[i]} == 2 )); then
      for (( j = i + 1; j < N_RET; ++j )); do
        T_RET[$j]=$(( T_RET[j] + 30 ))
      done

    # 3 fleets
    elif (( ${RET_COUNT[i]} == 3 )); then
      for (( j = i + 1; j < N_RET; ++j )); do
        T_RET[$j]=$(( T_RET[j] + 70 ))
      done
    fi

  done

  # Show adjusted schedule
  echo -e "\n$(date +%T) [Debug] Time schedule:"
  for (( i = 0; i < N_RET; ++i )); do
    echo "$i of 0..$((N_RET-1)): Time=${T_RET[i]}, #Fleets=${RET_COUNT[i]}, Fleet(s)=${LS_FLT[i]}"
  done

  #"""
  # Edit and read shiplist
  #"""

  if [ $(chk_yes $5) -eq 0 ]; then

    echo -e "\n$(date +%T) [Info] Edit ships to go 1-1..."

    edit_shipfile
    tmp_ships=$(read_shipfile)

    # put ships into array
    i=0
    for ship in $tmp_ships; do
        ships[i]=$ship
        (( ++i ))
    done

    # store ship count
    n_ships=$i

    # check ship list
    if [ "$n_ships" -eq 0 ]; then
      echo "$(date +%T) [Error] Bad shiplist! Quit expd_generic()"
      exit 1
    fi

    echo "$(date +%T) [Debug] Begin expedition..."
    countdown 3
  else
    echo -e "\n$(date +%T) [Debug] No sortie 1-1!"
  fi

  #"""
  # First Send
  #"""

  # supply and send
  echo "$(date +%T) [Debug] First send..."
  sidebar_supply
  supply_all_expd_fleets
  sidebar_expd
  expd_send3 $1 $2 $3

  # Ti must be set after first send
  Ti=$SECONDS
  echo "$(date +%T) [Debug] Set Ti = $Ti"

  # Turn off random time (critical)
  if [ "$DBG_RNDTIME" != "n" ]; then
    export DBG_RNDTIME=n
    echo "$(date +%T) [Info] Global random sleep effect turned off!"
  fi

  idx_ret=0
  echo "$(date +%T) [Info] Waiting for idx_ret=0 (~$(( T_RET[0] - T_EXPD_EARLY ))s)"


  #"""
  # Main Action Loop
  #"""

  # check every 30s if fleet return is approaching
  while (( idx_ret < N_RET )); do

    # check time elapsed doesn't enter warn region
    tmp_time=$(( T_RET[idx_ret] - T_EXPD_EARLY - ( SECONDS - Ti ) ))

    if (( tmp_time > T_EXPD_WARN )); then

      # Sortie condition:
      #   (1) sortie function is enabled
      #   (2) there are still ships to be processed
      #   (3) free time > 600s
      if [ $(chk_yes $5) -eq 0 ] && (( idx_ship < n_ships )) && (( tmp_time > 600 )); then
        ship_now=${ships[idx_ship]}
        echo "$(date +%T) [Info] Sending ship $ship_now ($((idx_ship + 1)) of $n_ships)"
        sidebar_hensei
        hensei_henkou 1 $ship_now
        # self-correction of shipid (2019.03)
        sortie_1_1_disintegrate4 2
        echo "$(date +%T) [Info] Ship $ship_now done!"
        (( ++idx_ship ))
      else
        # re-check time 30 seconds later
        sleep 30
      fi

    else

      # warn
      expd_sleep_warn_sidebar $tmp_time

      # get parameters
      tmp_time=${T_RET[idx_ret]}
      ls_flt_now="${LS_FLT[idx_ret]}"
      n_flt=${RET_COUNT[idx_ret]}
      echo "$(date +%T) [Debug] T_RET[$idx_ret]=$tmp_time, RET_COUNT[$idx_ret]=$n_flt, LS_FLT[$idx_ret]=$ls_flt_now"

      # receive
      expd_receive n $n_flt
      room_supply

      # supply and send
      if (( n_flt == 1 )); then
        # remove trailing space
        flt1=$( echo $ls_flt_now | xargs )

        # supply
        supply_fleet $flt1
        supply_all

        # re-send if last timepoint wasn't reached
        if (( idx_ret < N_RET - 1 )); then
          sidebar_expd
          expd_send1 $flt1 ${EXID[flt1]}
        fi

      elif (( n_flt == 2 )); then
        # parse fleet name(s)
        # cut delimiter=' ', field=1
        flt1=$( echo $ls_flt_now | cut -d ' ' -f 1 )
        flt2=$( echo $ls_flt_now | cut -d ' ' -f 2 )

        # supply
        supply_fleet $flt1
        supply_all
        supply_fleet $flt2
        supply_all

        # re-send if last timepoint wasn't reached
        if (( idx_ret < N_RET - 1 )); then
          sidebar_expd
          expd_send2 $flt1 ${EXID[flt1]} $flt2 ${EXID[flt2]}
        fi

      elif (( n_flt == 3 )); then

        # supply all
        supply_all_expd_fleets

        # re-send if last timepoint wasn't reached
        if (( idx_ret < N_RET - 1 )); then
          sidebar_expd
          expd_send3 $1 $2 $3
        fi

      else
        echo "$(date +%T) [Error] Bad n_flt: $n_flt"
        exit 1
      fi

      echo "$(date +%T) [Info] Finished idx_ret=${idx_ret}!"
      (( idx_ret++ ))
      echo "$(date +%T) [Info] Waiting for idx_ret=$idx_ret (~$(( T_RET[idx_ret] + Ti - SECONDS - T_EXPD_EARLY ))s)"
    fi
  done

  echo "$(date +%T) [Info] Recover global DBG_RNDTIME...."
  export DBG_RNDTIME=$DBG_RNDTIME_orig

  echo "$(date +%T) [Info] expd_generic($@) done!"
}

######################
# sortie/combat
######################

retreat_supply_all() {
  echo "$(date +%T) [Info] retreat_supply_all() begins..."
  combat_retreat
  room_supply
  supply_all
  echo "$(date +%T) [Info] retreat_supply_all() done!"
}


sortie() {
  #"""
  # Generic interactive sortie script (experimental)
  #
  # Args:
  #   $1  1~6  seaarea
  #   $2  1~6  world
  #"""

  # check args
  if [ $(chk_world $1 $2) -eq 1 ]; then
    echo "$(date $T) [Error] Bad world: $@"
    exit 1
  fi

  # arrange first combat
  echo -n "$(date +%T) Arrange first combat..."
  local flag_advance
  local flag_compass
  local flag_formation
  local flag_tough
  local flag_yasen
  local i

  read_safe flag_compass "Hit compass. How many times? (1~3/n)"
  read_safe flag_formation "Formation? (1~5/d, d=depends)"
  read_safe flag_tough "Tough combat? (y/n)"
  read_safe flag_yasen "Yasen? (n/y/d, d=depends)"

  # send
  echo "$(date +%T) [Info] sortie($@) begins..."
  countdown 3
  sidebar_to_world $1 $2

  # main loop
  while true; do

    ### go ###
    # compass
    if [ "$flag_compass" == "n" ]; then
      map_sleep_no_compass
    else
      i=1
      while [[ $i -le $flag_compass ]]; do
        map_compass
        (( i += 1 ))
      done
    fi

    # formation
    if [ "$flag_formation" == "d" ]; then
      combat_formation_choose
    else
      combat_formation $flag_formation
    fi

    # wait
    if [ $(chk_no $flag_tough) -eq 0 ]; then
      echo "$(date +%T) [Debug] sleep (ordinary combat)"
      combat_sleep_day 0 1 0 0 1 0 1 0
    else
      echo "$(date +%T) [Debug] sleep (tough combat)"
      combat_sleep_day 0 1 0 0 1 1 3 0
    fi

    # yasen
    if [ "$flag_yasen" == "d" ]; then
      combat_yasen_choose
    elif [ "$flag_yasen" == "y" ]; then
      combat_yasen_unsure
    elif [ "$flag_yasen" == "n" ]; then
      combat_no_yasen_unsure
    else
      echo "$(date +%T) [Debug] Bad flag_yasen: $flag_yasen, choose manually!"
    fi

    ### arrange next battle ###
    read_safe flag_advance "Advance? (n/y)"
    # if not "yes", leave map
    if [ $(chk_yes $flag_advance) -eq 1 ]; then
      echo "$(date +%T) [Info] Leave map!"
      break
    fi

    read_safe flag_compass "Hit compass. How many times? (0~3/n)"
    read_safe flag_formation "Formation? (1~5/d, d=depends)"
    read_safe flag_tough "Tough combat? (y/n)"
    read_safe flag_yasen "Yasen? (n/y/d, d=depends)"

    # advance
    if [ $(chk_yes $flag_advance) -eq 0 ]; then
      combat_advance $flag_compass
    fi

  done

  # post-combat
  combat_retreat
  room_supply

  echo "$(date +%T) [Debug] combat($@) done!"
}


sortie_1_1() {
  #"""
  # Args:
  #    $1   1~3  sortie N times
  #"""

  echo "$(date +%T) [Info] sortie_1_1($@) begins..."

  if [ -z "$1" ]; then
    echo "$(date +%T) [Debug] No sortie times, set to 1."
    local N=1
  elif (( $1 >= 1 )) && (( $1 <= 3 )); then
    local N=$1
  else
    echo "$(date +%T) [Error] Bad sortie count: $1"
    exit 1
  fi

  # supply (the ship may be just now selected from hensei list)
  sidebar_supply
  supply 1

  # go
  local i=1
  while (( i <= N )); do

    echo "$(date +%T) [Info] loop $i of $N begins..."
    ## 1. enter combat
    sidebar_to_world 1 1

    ## 2. combat ##
    # 1st point
    map_sleep_no_compass
    # no formation
    # sleep 13 + 2 shell + close torpedo + end_to_rank = 13 + 4 + 7 + 11 = 35
    # day S almost surely, but sometimes need closing torpedo
    echo "$(date +%T) [Info] 1-1-A combat begins...(35s)"
    sleep 35
    echo "$(date +%T) [Info] 1-1-A combat done!"
    combat_report
    combat_advance

    # 2nd point
    map_compass
    # no formation
    # B: sleep (point A) + 1 shell + middmg = 35 + 2 + 5 = 42
    # C: sleep (point A) + 3 shell + middmg - end_to_rank = 35 + 6 + 5 - 11 = 35
    echo "$(date +%T) [Info] 1-1-B/C combat begins...(42s)"
    sleep 42
    echo "$(date +%T) [Info] 1-1-B/C combat done!"
    combat_no_yasen_unsure
    # no advance

    ## 3. disintegrate ##
    room_disintegrate_top 2

    ## 4. supply ##
    sidebar_supply
    supply 1
    echo "$(date +%T) [Info] loop $i of $N done!"

    (( i += 1 ))
  done

  echo "$(date +%T) [Info] sortie_1_1($@) done!"
}


sortie_1_1_no_disintegrate() {
  #"""
  # Args:
  #    $1 (int):  sortie N times (1~3)
  #"""

  echo "$(date +%T) [Info] sortie_1_1_no_disintegrate($@) begins..."

  if [ -z "$1" ]; then
    echo "$(date +%T) [Debug] No sortie count. Set to 1."
    local N=1
  elif (( $1 >= 1 )) && (( $1 <= 3 )); then
    local N=$1
  else
    echo "$(date +%T) [Error] Bad sortie count: $1"
    exit 1
  fi

  # supply (the ship may be just now selected from hensei list)
  sidebar_supply
  supply 1

  # go
  local i=1
  while (( i <= N )); do

    echo "$(date +%T) [Info] loop $i of $N begins..."
    ## 1. enter combat
    sidebar_to_world 1 1

    ## 2. combat ##
    # 1st point
    map_sleep_no_compass
    # no formation
    # sleep 13 + 2 shell + close torpedo + end_to_rank = 13 + 4 + 7 + 11 = 35
    # day S almost surely, but sometimes need closing torpedo
    echo "$(date +%T) [Info] 1-1-A combat begins...(35s)"
    sleep 35
    echo "$(date +%T) [Info] 1-1-A combat done!"
    combat_report
    combat_advance

    # 2nd point
    map_compass
    # no formation
    # B: sleep (point A) + 1 shell + middmg = 35 + 2 + 5 = 42
    # C: sleep (point A) + 3 shell + middmg - end_to_rank = 35 + 6 + 5 - 11 = 35
    echo "$(date +%T) [Info] 1-1-B/C combat begins...(42s)"
    sleep 42
    echo "$(date +%T) [Info] 1-1-B/C combat done!"
    combat_no_yasen_unsure
    # no advance

    ## 3. disintegrate ##
    echo "$(date +%T) [Info] No disintegration!"

    ## 4. supply ##
    room_supply
    supply 1
    echo "$(date +%T) [Info] loop $i of $N done!"

    (( i += 1 ))
  done

  echo "$(date +%T) [Info] sortie_1_1_no_disintegrate($@) done!"
}


sortie_1_1_disintegrate4() {
  #"""
  # Disintegrate 4 times as self-correction of ship id.
  # Designed specifically for mass-kira
  #
  # Args:
  #    $1   1~3  sortie N times
  #"""

  echo "$(date +%T) [Info] sortie_1_1($@) begins..."

  if [ -z "$1" ]; then
    echo "$(date +%T) [Debug] No sortie times, set to 1."
    local N=1
  elif (( $1 >= 1 )) && (( $1 <= 3 )); then
    local N=$1
  else
    echo "$(date +%T) [Error] Bad sortie count: $1"
    exit 1
  fi

  # supply (the ship may be just now selected from hensei list)
  sidebar_supply
  supply 1

  # go
  local i=1
  while (( i <= N )); do

    echo "$(date +%T) [Info] loop $i of $N begins..."
    ## 1. enter combat
    sidebar_to_world 1 1

    ## 2. combat ##
    # 1st point
    map_sleep_no_compass
    # no formation
    # sleep 13 + 2 shell + close torpedo + end_to_rank = 13 + 4 + 7 + 11 = 35
    # day S almost surely, but sometimes need closing torpedo
    echo "$(date +%T) [Info] 1-1-A combat begins...(35s)"
    sleep 35
    echo "$(date +%T) [Info] 1-1-A combat done!"
    combat_report
    combat_advance

    # 2nd point
    map_compass
    # no formation
    # B: sleep (point A) + 1 shell + middmg = 35 + 2 + 5 = 42
    # C: sleep (point A) + 3 shell + middmg - end_to_rank = 35 + 6 + 5 - 11 = 35
    echo "$(date +%T) [Info] 1-1-B/C combat begins...(42s)"
    sleep 42
    echo "$(date +%T) [Info] 1-1-B/C combat done!"
    combat_no_yasen_unsure
    # no advance

    ## 3. disintegrate ##
    # Theoretical value = 4
    room_disintegrate_top 4

    ## 4. supply ##
    sidebar_supply
    supply 1
    echo "$(date +%T) [Info] loop $i of $N done!"

    (( i += 1 ))
  done

  echo "$(date +%T) [Info] sortie_1_1($@) done!"
}


sortie_1_5() {
  #"""
  # World 1-5 semi-automatic sortie script
  #"""
  echo "$(date +%T) [Info] sortie_1_5() begins..."

  sidebar_to_world 1 5

  #"""point A"""
  map_sleep_no_compass
  combat_formation 5
  combat_sleep_day 0 0 0 1 0 0 1
  # not 100% day S
  combat_no_yasen_unsure
  combat_advance_choose

  #"""point B"""
  map_sleep_no_compass
  combat_formation 5
  combat_sleep_day 0 0 0 1 0 0 1
  combat_no_yasen_unsure
  combat_advance_choose

  #"""point C"""
  map_compass
  combat_formation 5
  combat_sleep_day 0 0 0 1 1 0 1
  combat_no_yasen_unsure
  combat_advance_choose

  #"""point E (no combat)"""
  map_compass

  #"""point I (boss)"""
  map_compass
  combat_formation 5
  combat_sleep_day 0 0 0 1 1 0 1
  combat_no_yasen_unsure

  #"""back to room"""
  room_supply
  echo "$(date +%T) [Info] sortie_1_5() done!"
}


sortie_3_4_bottom() {
  #"""
  # World 3-4 bottom route semi-auto sortie script.
  # Assumed formation: 2CL+3DD+1CA(V)
  #"""

  echo "$(date +%T) [Info] sortie_3_4_bottom begins..."

  # initial supply
  sidebar_supply
  supply_all
  sidebar_to_world 3 4

  # start -> D (time is OK for maelstrom)
  map_compass
  # D -> H
  map_sleep_no_compass
  combat_formation 1
  combat_sleep_day 0 0 0 0 1 0 2
  combat_no_yasen_unsure
  combat_advance_choose
  # H -> L
  map_compass
  # L -> J
  map_compass
  # J -> P
  map_sleep_no_compass
  combat_formation 1
  # dat combat can go really bad
  local t=$(( T_COMBAT_DAY_WORST / 1000 ))
  echo "$(date +%T) [Info] Sleep for T_COMBAT_DAY_WORST=${t}s."
  sleep $t
  # assume 6 (heavy dmg + CI) conservatively
  combat_yasen_unsure 6

  # room
  room_supply
  echo "$(date +%T) [Info] sortie_3_4_bottom done!"
}


sortie_5_2_C() {
  #"""
  # World 5-2-C full auto sortie script (for training SS)
  #
  # Args:
  #    $1  1..  sortie count
  #"""

  echo "$(date +%T) [Info] sortie_5_2_C($@) begins..."

  local i=1

  # initial location
  sidebar_supply

  while (( i <= $1 )); do

    # supply each 4 sorties
    if (( i % 4 == 1 )); then
      # The first time we start from sidebar. Otherwise from room.
      if (( i > 1 )); then
        room_supply
      fi
      supply_all
      sidebar_room
    fi

    echo "$(date +%T) [Info] Sortie $i of $1 begins..."
    room_spe
    spe_sortie
    sortie_world 5 2
    sortie_go
    # Actual sortie-to-report time: 50s
    # start -> A (time is OK for maelstrom)
    map_compass
    # A -> B
    map_sleep_no_compass
    # B -> C
    map_sleep_no_compass
    # C
    echo "$(date +%T) [Info] Sleep 30s (MRY-specific)..."
    sleep 30
    combat_report_nodrop
    # to be extra safe on retreat (it sometimes get really slow)
    combat_retreat
    sleep 3

    echo "$(date +%T) [Info] Sortie $i of $1 done!"
    (( i += 1 ))
  done

  # room
  room_supply
  echo "$(date +%T) [Info] sortie_5_2_C($@) done!"
}


sortie_5_2_C_to20() {
  #"""
  # Raise MRY to Lv.20 using 5-2-C
  #
  # Args:
  #    $1  ship   shipno
  #    $2  1..19  current lv
  #    $3  int    exp to next level
  #"""

  echo "$(date +%T) [Info] sortie_5_2_C_to20($@) begins..."
  # check variables
  if [ $(chk_shiplistno $1) -eq 1 ]; then
    echo "$(date +%T) [Error] Bad ship: $1"
    exit 1
  fi
  if [[ ! ( $2 -ge 1 && $2 -le 19 ) ]]; then
    echo "$(date +%T) [Error] Bad level: $2"
    exit 1
  fi
  if [[ ! ( $3 -ge 1 && $3 -le $(($2 * 100)) ) ]]; then
    echo "$(date +%T) [Error] Bad exp: $3"
    exit 1
  fi

  # switch ship
  sidebar_hensei
  # extra safety!
  hensei_dismiss
  hensei_henkou 1 $1

  # Expected time to sortie
  # ( (19+(lv+1) ) * 100 * (19-lv) / 2 + exp_next) / 540 + 1
  local N=$(( ( (20 + $2) * 50 * (19 - $2) + $3) / 540 + 1 ))
  sortie_5_2_C $N
  echo "$(date +%T) [Info] sortie_5_2_C_to20($@) done!"
}


sortie_5_3_P() {
  #"""
  # World 5-3-P semi-automatic sortie script
  #"""

  # Warning (5-3 drops are good for modernization)
  echo "$(date +%T) [Info] Note: NO disintegration after fleet return!"

  echo "$(date +%T) [Info] sortie_5_3_P() begins..."

  sidebar_supply
  supply_all
  sidebar_to_world 5 3
  # start -> D
  map_compass
  # D -> G
  map_sleep_no_compass
  # G -> I
  map_compass
  # I
  combat_formation 1
  combat_sleep_night_6v6
  combat_report
  combat_advance_choose

  # I -> O
  map_compass
  # O -> P (choose)
  echo "$(date +%T) [Info] Choose O -> P..."
  zmcs 620 11 309 13 1 $T_MAP_COMPASS

  # P
  combat_formation 1
  combat_sleep_night_6v6
  combat_report
  combat_retreat

  # room
  room_supply
  echo "$(date +%T) [Info] sortie_5_3_P() done!"
}


sortie_6_5_B_first() {
  #"""
  # World 6-5-B one-button startup
  #"""

  echo "$(date +%T) [Info] sortie_6_5_B_first() begins..."

  sidebar_supply
  supply_all
  sidebar_room
  room_spe
  spe_sortie
  sortie_seaarea 6
  # supply lbas
  sortie_lbas_supply_wings 1 2
  # then enter map 5
  sortie_map 5
  sortie_decide
  sortie_go
  # send lbas
  zmcs 639 11 134 10 1 $T_CLICK_ANIMATE
  zmcs 639 11 134 10 1 $T_CLICK_ANIMATE
  map_lbas_confirm
  zmcs 639 11 134 10 1 $T_CLICK_ANIMATE
  zmcs 639 11 134 10 1 $T_CLICK_ANIMATE
  map_lbas_confirm
  sleep 1
  # start -> B
  map_sleep_no_compass
  # B
  combat_formation 5
  # (optional post-combat action: 5SS, 2 rounds)
  combat_sleep_day 0 0 0 1 1 0 1
  combat_report

  echo "$(date +%T) [Info] sortie_6_5_B_first() done!"
}


edit_shipfile() {
  #"""
  # Edit $SHIPFILE defined in project config (interactive)
  #"""
  echo "$(date +%T) [Info] edit_shipfile() begins..."

  local res
  read_safe res "Edit ship list file using vim? (y/n)"
  if [ $(chk_yes "$res") -eq 0 ]; then
    if [ ! -e "$SHIPFILE" ]; then
      echo "$(date +%T) [Info] $SHIPFILE does not exist. Copy from template."
      cp -f "$SHIPFILE_TEMPLATE" "$SHIPFILE"
    fi
    vim "$SHIPFILE"
  else
    echo "$(date +%T) [Info] Please edit $SHIPFILE manually!"
    touch "$SHIPFILE"
  fi

  echo "$(date +%T) [Info] edit_shipfile() done!"
}

read_shipfile() {
  #"""
  # Read and check $SHIPFILE
  #
  # Returns: list of ships
  #"""

  # logic: read file       | uncomment  | format to: "arg1 arg2 ... argN"
  local ships=$( cat "$SHIPFILE" | sed '1,5d' | xargs )
  local ship
  # check ship numbers
  for ship in $ships; do
    if [ $(chk_shiplistno $ship) -eq 1 ]; then
      exit 1
    fi
  done

  # return
  echo $( cat "$SHIPFILE" | sed '1,5d' | xargs )
}

######################
# quest
######################

quest_daily_factory() {
  #"""
  # Args:
  #    $1~$4: development recipe (default= all 10)
  #"""

  echo "$(date +%T) [Info] quest_daily_factory($@) begins..."

  ## command args
  if [ $(chk_develop_resource $1) -eq 0 ]; then
    local fuel=$1
  else
    echo "$(date +%T) [Debug] Bad fuel: $1, set to 10"
    local fuel=10
  fi

  if [ $(chk_develop_resource $2) -eq 0 ]; then
    local ammo=$2
  else
    echo "$(date +%T) [Debug] Bad ammo: $2, set to 10"
    local ammo=10
  fi

  if [ $(chk_develop_resource $3) -eq 0 ]; then
    local steel=$3
  else
    echo "$(date +%T) [Debug] Bad steel: $3, set to 10"
    local steel=10
  fi

  if [ $(chk_develop_resource $4) -eq 0 ]; then
    local baux=$4
  else
    echo "$(date +%T) [Debug] Bad baux: $4, set to 10"
    local baux=10
  fi

  ## 1. dev*1 quest
  echo "$(date +%T) [Info] quest dev*1 begins..."
  # undertake
  room_quest_daily_item 5
  quest_room
  # do
  room_factory
  develop $fuel $ammo $steel $baux 1
  # complete
  room_quest_daily_item 5
  quest_complete
  echo "$(date +%T) [Info] quest dev*1 done!"

  ## 2. build*1 quest
  echo "$(date +%T) [Info] quest build*1 begins...."
  # undertake
  quest_item 5
  quest_room
  # do
  room_factory
  build 1 y 30 30 30 30
  # complete
  room_quest_daily_item 5
  quest_complete
  echo "$(date +%T) [Info] quest build*1 done!"

  ## 3. dev*3 quest
  echo "$(date +%T) [Info] quest dev*3 begins...."
  # undertake
  quest_item 5
  quest_room
  # do
  room_factory
  develop $fuel $ammo $steel $baux 3
  # complete
  room_quest_daily_item 5
  quest_complete
  echo "$(date +%T) [Info] quest dev*3 done!"

  ## 4. build*3 quest
  echo "$(date +%T) [Info] quest build*3 begins..."
  # undertake
  quest_item 5
  quest_room
  # do
  room_factory
  # 1) launch slot 1
  build_launch 1
  # 2) build slot 1 (fast)
  build 1 y 30 30 30 30
  # 3) build slot 2
  build 2 n 30 30 30 30
  # 4) take out slot 1 again
  # fast construction may not complete!
  sleep 3
  build_launch 1
  # 5) build slot 1
  build 1 n 30 30 30 30
  # complete
  room_quest_daily_item 5
  quest_complete
  echo "$(date +%T) [Info] quest build*3 done!"

  ## 5. disintegrate*2 quest
  echo "$(date +%T) [Info] quest disintegrate*2 begins..."
  # undertake
  quest_item 5
  quest_room
  # do
  room_disintegrate_top 2
  # complete
  room_quest_daily_item 5
  quest_complete
  echo "$(date +%T) [Info] quest disintegrate*2 done!"

  ## 6. undertake improvement Mission

  # undertake
  quest_item 5
  echo "$(date +%T) [Info] improvement quest is undertaken"
  quest_room
  room_hensei

  echo "$(date +%T) [Info] quest_daily_factory($@) done!"
}


quest_daily_sortie() {
  #"""
  # Sortie ship $2, $3, $4 to W1-1 twice; $5 once
  #
  # Args:
  #    $1     y/n   undertake A-Gou Sakusen
  #    $2     XXYY  ship No. for 1+1 sortie
  #    $3~$5  XXYY  ship No. for 10 combats
  #"""

  echo "$(date +%T) [Info] quest_daily_sortie($@) begins...."

  local agou
  local i
  local flag

  ## args
  # A-Gou
  if [ $(chk_yes $1) -eq 0 ]; then
    echo "$(date +%T) [Debug] A-Gou Sakusen will be undertaken"
    agou=y
  elif [ $(chk_no $1) -eq 0 ]; then
    echo "$(date +%T) [Debug] A-Gou Sakusen will be ignored"
    agou=n
  else
    echo "$(date +%T) [Warning] Bad A-Gou Sakusen: $1, set to n"
    agou=n
  fi

  # check ships
  flag="y"
  for i in {2..5}; do
    if [ $(chk_shiplistno ${!i}) -eq 1 ]; then
      flag="n"
    fi
  done

  if [ "$flag" == "y" ]; then
    local ship1=$2
    local -a ship10=( 0 $3 $4 $5 )
  else
    echo "$(date +%T) [Error] Bad ship list: $2 $3 $4 $5"
    exit 1
  fi


  ## main

  # 1-1. undertake
  echo "$(date +%T) [Info] doing 1st sortie quest..."
  room_quest_daily_item 1
  quest_room

  # 1-2. do
  room_hensei
  hensei_henkou 1 $ship1
  sortie_1_1 1

  # 1-3. complete quest
  room_quest_daily_item 1
  quest_complete
  echo "$(date +%T) [Info] 1st sortie quest done!"

  # 2-1. undertake 2nd sortie
  echo "$(date +%T) [Info] doing 2nd sortie quest..."
  quest_item 1
  quest_room

  # 2-2. do
  room_hensei
  sortie_1_1 1

  # 2-3.complete quest
  room_quest_daily_item 1
  quest_complete
  echo "$(date +%T) [Info] 2nd sortie quest done!"

  # 3-1. undertake 10 combats mission
  # 10 combats
  quest_item 1
  # A-Gou
  if [ "$agou" == "y" ]; then
    quest_weekly
    quest_item 2
    echo "$(date +%T) [Info] A-Gou Sakusen is undertaken!"
  fi
  quest_room
  room_hensei

  # 3-2. sortie - 2 times each
  local i=1
  while (( i <= 3 )); do
    echo "$(date +%T) [Info] 10 combats mission: ship $i: ${ship10[i]}"
    sidebar_hensei
    hensei_henkou 1 ${ship10[i]}

    # sortie once for the last ship
    if (( i <= 2 )); then
      echo "$(date +%T) [Debug] i = $i, sortie 2 times"
      # auto-correction just in case
      sortie_1_1_disintegrate4 2
    else
      echo "$(date +%T) [Debug] i = $i, sortie 1 times"
      sortie_1_1 1
    fi
    echo "$(date +%T) [Debug] i = $i after sortie"

    (( i += 1 ))
  done

  # 3-3. complete quest
  room_quest_daily_item 1
  quest_complete
  echo "$(date +%T) [Info] 10 combats mission done!"

  quest_room
  room_supply
  echo "$(date +%T) [Info] quest_daily_sortie($@) done!"
}


quest_daily_sortie2() {
  #"""
  # do the first 2 daily sortie mission
  #"""

  echo "$(date +%T) [Info] quest_daily_sortie2() begins...."

  # 1-1. undertake quest
  echo "$(date +%T) [Info] doing 1st sortie quest..."
  room_quest_daily_item 1
  quest_room

  # 1-2. do
  room_supply
  sortie_1_1 1

  # 1-3. complete quest
  room_quest_daily_item 1
  quest_complete
  echo "$(date +%T) [Info] 1st sortie quest done!"

  # 2-1. undertake 2nd sortie
  echo "$(date +%T) [Info] doing 2nd sortie quest..."
  quest_item 1
  quest_room

  # 2-2. do
  room_supply
  sortie_1_1 1

  # 2-3.complete quest
  room_quest_daily_item 1
  quest_complete
  echo "$(date +%T) [Info] 2nd sortie quest done!"

  # finishing
  quest_room
  room_supply
  supply 1
  room_hensei

  echo "$(date +%T) [Info] quest_daily_sortie2() done!"
}


quest_daily_sortie10() {
  #"""
  # Sortie ship $2, $3 to W1-1 twice; $4 once.
  # Flagship must not equal $2.
  #
  # Args:
  #    $1     y/n   undertake A-Gou Sakusen
  #    $2~$4  XXYY  ship No. for 10 combats
  #"""

  echo "$(date +%T) [Info] quest_daily_sortie10($@) begins...."

  ##### command args #####
  # A-Gou
  if [ $(chk_yes $1) -eq 0 ]; then
    echo "$(date +%T) [Debug] A-Gou Sakusen will be undertaken"
    local agou=y
  elif [ $(chk_no $1) -eq 0 ]; then
    echo "$(date +%T) [Debug] A-Gou Sakusen will be ignored"
    local agou=n
  else
    echo "$(date +%T) [Warning] Bad A-Gou Sakusen: $1, set to n"
    local agou=n
  fi

  # check ships
  if [ $(chk_shiplistno $2) -eq 0 ] && [ $(chk_shiplistno $3) -eq 0 ] && \
     [ $(chk_shiplistno $4) -eq 0 ]; then
    local -a ship10=( 0 $2 $3 $4 )
  else
    echo "$(date +%T) [Error] Bad ship list: $2 $3 $4"
    exit 1
  fi

  ##### main #####
  # 1. undertake
  room_quest_daily_item 1

  # A-Gou
  if [ "$agou" == "y" ]; then
    quest_weekly
    quest_item 2
    echo "$(date +%T) [Info] A-Gou Sakusen is undertaken!"
  fi
  quest_room
  room_hensei

  # sortie
  local i=1
  while (( i <= 3 )); do
    echo "$(date +%T) [Info] 10 combats mission: ship $i: ${ship10[i]}"
    sidebar_hensei
    hensei_henkou 1 ${ship10[i]}

    # sortie once for the last ship
    if (( i <= 2 )); then
      echo "$(date +%T) [Debug] i = $i, sortie 2 times"
      sortie_1_1 2
    else
      echo "$(date +%T) [Debug] i = $i, sortie 1 times"
      sortie_1_1 1
    fi
    echo "$(date +%T) [Debug] i = $i after sortie"

    (( i += 1 ))
  done

  # 3-3. complete quest
  room_quest_daily_item 1
  quest_complete
  echo "$(date +%T) [Info] 10 combats mission completed!"

  quest_room
  room_supply
  supply 1
  echo "$(date +%T) [Info] quest_daily_sortie10($@) done!"
}

practice_1() {
  #"""
  # Practice one fleet
  #
  # Args:
  #    $1  rival slot
  #    $2  formation
  #    $3  sleep worst case? (y/n)
  #"""
  echo "$(date +%T) [Info] practice_1($@) begins..."

  sidebar_to_practice $1 $2

  # sleep worst case of not
  if [ $( chk_no $3 ) -eq 0 ]; then
    echo "$(date +%T) [Info] Sleep T_COMBAT_DAY=$(( T_COMBAT_DAY / 1000 ))s ..."
    sleep $(( T_COMBAT_DAY / 1000 ))
  else
    echo "$(date +%T) [Info] Sleep T_COMBAT_DAY_WORST=$(( T_COMBAT_DAY_WORST / 1000 ))s ..."
    sleep $(( T_COMBAT_DAY_WORST / 1000 ))
  fi

  # always yasen (practice mode)
  combat_yasen_unsure_practice

  # room
  room_supply
  supply_all

  echo "$(date +%T) [Info] practice_1($@) done!"
}


practice_5() {
  #"""
  # 1-button consecutive practice
  #
  # Args:
  #    $1,$2        rival slot, formation
  #    $3,$4        rival slot, formation
  #    $5,$6        rival slot, formation
  #    $7,$8        rival slot, formation
  #    $9,$10       rival slot, formation
  #"""

  echo "$(date +%T) [Info] practice_5($@) begins..."
  countdown 3
  # do all
  practice_1 $1 $2 y
  practice_1 $3 $4 y
  practice_1 $5 $6 y
  practice_1 $7 $8 y
  practice_1 $9 ${10} y

  echo "$(date +%T) [Info] practice5($@) done!"
}


quest_daily_practice_early() {
  #"""
  # 1-button solver of practice quest - early half
  #
  # Args:
  #    $1      y/n  process quest or not
  #    $2,$3        rival slot, formation
  #    $4,$5        rival slot, formation
  #    $6,$7        rival slot, formation
  #    $8,$9        rival slot, formation
  #    $10,$11      rival slot, formation
  #"""

  echo "$(date +%T) [Info] quest_daily_practice_early($@) begins..."

  # undertake
  if [ $(chk_yes $1) -eq 0 ]; then
    echo "$(date +%T) [Info] undertaking 3 practice quest..."
    room_quest_daily_item 2
    quest_room
    room_supply
  fi

  # 1~3
  practice_1 $2 $3 y
  practice_1 $4 $5 y
  practice_1 $6 $7 y

  if [ $(chk_yes $1) -eq 0 ]; then
    echo "$(date +%T) [Info] undertaking 5 practice win quest..."
    room_quest_daily_item 2
    quest_complete
    quest_item 2
    quest_room
    room_supply
  fi

  # 4-5
  practice_1 $8 $9 y
  practice_1 ${10} ${11} y

  echo "$(date +%T) [Info] quest_daily_practice_early($@) done!"
}
