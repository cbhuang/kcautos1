#!/usr/bin/env bash
#"""
# Sortie Actions
#
# Anything BEFORE entering a map is coded here.
#"""

# dependencies
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/func.sh"


#"""
# top bar
#"""


sortie_practice() {
  echo "$(date +%T) [Debug] sortie_practice()..."
  zmcs 623 65 110 15  1 $T_CLICK_API_ANIMATE
}


sortie_expd() {
  echo "$(date +%T) [Debug] sortie_expd()..."
  zmcs 709 64 112 12  1 $T_CLICK_API_ANIMATE
}


sortie_lbas() {
  #"""
  # Hit LBAS button in the top bar (seaarea 6 only)
  #"""
  echo "$(date +%T) [Debug] sortie_lbas()..."
  zmcs 436 85 112 14  1 $T_CLICK_ANIMATE
}


#"""
# Land-Based Aircraft Support (LBAS)
#"""
sortie_lbas_wing() {
  #"""
  # Args:
  #   $1 (int): wing 1~3
  #"""
  echo "$(date +%T) [Debug] sortie_lbas_wing($@)..."
  zmcs $(( 540 + 60 * $1 )) 50 113 10 1 $T_CLICK_INSTANT
}


sortie_lbas_squad() {
  #"""
  # Args:
  #   $1 (int): squadrons 1~4
  #"""
  echo "$(date +%T) [Debug] sortie_lbas_squad($@)..."
  zmcs 591 185 $(( 181 + 59 * $1 )) 30 1 $T_CLICK_INSTANT
}


sortie_lbas_status() {
  #"""
  # Flip landbase status (may fail!)
  #
  # Args:
  #   $1 (int): times to hit
  #"""
  echo "$(date +%T) [Debug] sortie_lbas_status($@)..."

  if [ -z "$1" ]; then
    local N=1
  else
    local N=$1
  fi

  local i=1
  while (( i <= N )); do
    echo "$(date +%T) [Debug]    click status ($i of $1)"
    zmscs 765 16 140 35 600 1 $T_CLICK_API_ANIMATE
    (( i += 1 ))
  done
}


sortie_lbas_supply_wing() {
  #"""
  # supply wing (all 4 squadrons)
  #"""
  echo "$(date +%T) [Debug] sortie_lbas_supply_wing()..."
  #zmscs 765 18 209 10 600 1 $T_CLICK_API_ANIMATE
  zmcs 765 18 209 10 1 $T_CLICK_API_ANIMATE
}


sortie_lbas_leave() {
  #"""
  # leave LBAS interface
  #"""
  echo "$(date +%T) [Debug] sortie_lbas_leave()..."
  # re-click the sortie_lbas button
  sortie_lbas
  # or re-click anywhere on sortie area but not the sidebar
  #zmcs 172 384 152 278 1 $T_CLICK_ANIMATE
}

sortie_lbas_supply_wings() {
  #"""
  # Supply wings and leave (integrated function)
  #
  # Args:
  #   $1.. (int):  which wings to supply
  #"""
  echo "$(date +%T) [Debug] sortie_lbas_supply_wings($@) begins..."
  if (( $# == 0 )); then
    echo "$(date +%T) [Error] no argument provided!"
    exit 1
  fi
  local wing
  # enter
  sortie_lbas
  # supply
  for wing in "$@" ; do
    sortie_lbas_wing $wing
    sortie_lbas_supply_wing
  done
  # leave
  sortie_lbas_leave
  echo "$(date +%T) [Debug] sortie_lbas_supply_wings($@) done!"
}


#"""
# Seaarea and Map
#"""


sortie_seaarea() {
  #"""
  # Enter a seaarea
  #
  # Args:
  #   $1 (int): Sea Area 1~6
  #"""

  echo "$(date +%T) [Debug] sortie_seaarea($@)..."

  # check
  if [ $(chk_seaarea $1) -eq 1 ]; then
    echo "$(date +%T) [Error] Bad Sea Area: $1"
    exit 1
  fi

  if (( $1 == 1 )); then
    zmcs 127 40 432 29  1 $T_CLICK_INSTANT
  elif (( $1 == 2 )); then
    zmcs 195 35 432 25  1 $T_CLICK_INSTANT
  elif (( $1 == 3 )); then
    zmcs 256 39 434 22  1 $T_CLICK_INSTANT
  elif (( $1 == 7 )); then
    zmcs 320 38 434 22  1 $T_CLICK_INSTANT
  elif (( $1 == 4 )); then
    zmcs 384 37 434 22  1 $T_CLICK_INSTANT
  elif (( $1 == 5 )); then
    zmcs 446 40 435 20  1 $T_CLICK_INSTANT
  elif (( $1 == 6 )); then
    zmcs 510 39 435 21  1 $T_CLICK_INSTANT
  else
    echo "$(date +%T) [Error] Unimplemented Sea Area: $1"
    exit 1
  fi
}

sortie_eo() {
  #"""
  # Enter EO map page
  #"""
  echo "$(date +%T) [Debug] Enter EO map page..."
  zmcs 695 44 256 38 1 $T_CLICK_INSTANT
}

sortie_map() {
  #"""
  # Hit a map
  #
  # Args:
  #   $1 (int): map 1~6
  #"""

  echo "$(date +%T) [Debug] sortie_map($@)..."

  if [ $(chk_map $1) -eq 0 ]; then
    case "$1" in
      "1")
        zmcs 195 204 170 80 1 $T_CLICK_INSTANT
        ;;
      "2")
        zmcs 487 130 156 92 1 $T_CLICK_INSTANT
        ;;
      "3")
        zmcs 160 204 304 84 1 $T_CLICK_INSTANT
        ;;
      "4")
        zmcs 644 112 366 38 1 $T_CLICK_INSTANT
        ;;
      "5")
        sortie_eo
        zmcs 210 552 149 69 1 $T_CLICK_INSTANT
        ;;
      "6")
        sortie_eo
        zmcs 210 554 244 71 1 $T_CLICK_INSTANT
        ;;
      "7")
        echo "$(date +%T) [Warning] Map 7 is not implemented!"
        sortie_eo
        zmcs 212 554 340 70 1 $T_CLICK_INSTANT
        ;;
    esac
  else
    echo "$(date +%T) [Error] Bad Map: $1"
    exit 1
  fi
}


sortie_decide() {
  #"""
  # Hit decide button after picking a map (not out yet)
  #"""
  echo "$(date +%T) [Info] sortie_decide()..."
  zmcs 606 160 431 27 1 $T_CLICK_ANIMATE
}


sortie_world() {
  #"""
  # Aggregate function to sortie world X-Y
  #
  # Args:
  #   $1  1~6  Sea Area
  #   $2  1~6  Map
  #"""

  echo "$(date +%T) [Info] sortie_world($@)..."

  if [ $(chk_world $1 $2) -eq 0 ]; then
    sortie_seaarea $1
    sortie_map $2
    sortie_decide
  else
    echo "$(date +%T) [Error] Bad World: $1-$2"
    exit 1
  fi
}


sortie_fleet() {
  #"""
  # Pick the fleet to sortie
  #
  # Args:
  #  $1 (int): fleet (1~4)
  #"""
  echo "$(date +%T) [Debug] sortie_fleet($@)..."

  if [ $(chk_fleet_id $1) -eq 0 ]; then
    # pick fleet
    zmcs $(( 322 + 31 * $1 )) 10 107 9  1 $T_CLICK_ANIMATE
  else
    echo "$(date +%T) [Error] Bad fleet: $1"
    exit 1
  fi
}


sortie_go() {
  #"""
  # Send (confirm) sortie
  #
  # NOTE: Button position should be compatible with (6-4, 6-5)
  #"""
  echo "$(date +%T) [Info] sortie_go() begins..."
  zmcs 543 140 430  13 1  7500
  echo "$(date +%T) [Info] sortie_go() done!"
}
