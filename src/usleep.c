#include <stdio.h>  // printf
#include <unistd.h>  // usleep
#include <stdlib.h>  // strtol(*char, end, base)

int main (int argc, const char *argv[]) {

    // check argument count
    if (argc != 2) {
        printf("#argument != 1");
        return 1;
    }

    // parse input
    int us = strtol(argv[1], NULL, 10);

    // sleep
    usleep(us);

    return 0;
 }

