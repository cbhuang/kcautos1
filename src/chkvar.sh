#!/usr/bin/env bash
#"""
# Variable check functions.
#
# Most functions return these 2 values by ``echo``:
#
# * 0 = good
# * 1 = bad
#
# USAGE:
#
# .. code-block:: bash
#
#    # capture the returned value command substitution
#    var=$(check_func $arg1 $arg2 .... $argN)
#
# NOTE:
#    Mind quoting problems within [[ ]] !!
#    Always define variables outside and write [[ $var1 op $var2 ]]
#    DO NOT QUOTE! It always fails!
#"""

#"""
# Generic
# ====================
#"""

chk_yes() {
  # Args:
  #   $1  y/other
  # Returns:
  #   0: y or Y
  #   1: null or other

  local re='^[yY]$'
  if [[ $1 =~ $re ]]; then
    echo 0
  else
    echo 1
  fi
}


chk_no() {
  # Args:
  #   $1  y/other
  # Returns:
  #   0: y or Y
  #   1: null or other

  local re='^[nN]$'
  if [[ $1 =~ $re ]]; then
    echo 0
  else
    echo 1
  fi
}


chk_num() {
  #""" check pure numeric text
  #
  # pareameters
  #   $1   any text
  # Returns:
  #    0   is numeric (can have leading zeroes)
  #    1   is not
  #"""

  local re="^[0-9]+$"

  if [[ $1 =~ $re ]]; then
    echo 0
  else
    echo 1
  fi
}


chk_ltnum() {
  #""" check letter-num ID
  # paramters
  #   $1   any text
  # Returns:
  #   0    is A1~Z999, num can have leading zeros
  #   1    is not
  #"""

  local re='^[A-Z][0-9]{1,3}$'

  if [[ $1 =~ $re ]]; then
    echo 0
  else
    echo 1
  fi
}


chk_genid() {
  #""" check generic ID
  # paramters
  #   $1  any text
  # Returns:
  #   0   is 001~999 / A1~Z999, num can have leading zeros
  #   1   is not
  #"""

  local re='^[A-Z]?[0-9]{1,3}$'

  if [[ $1 =~ $re ]]; then
    echo 0
  else
    echo 1
  fi
}


chk_fleet_id() {
  # Args:
  #   $1  1-4
  # Returns:
  #   0: 1-4
  #   1: null or other

  local re='^[1-4]$'
  if [[ $1 =~ $re ]]; then
    echo 0
  else
    echo 1
  fi
}


#"""
# Hensei
# ====================
#"""

chk_shiplistno() {
  # Args:
  #   $1  XXYY  see mass-kira.txt
  # Returns:
  #   0: XX=1,2,...99 and YY=01~10
  #   1: null or other

  local re='^[0-9]{3,4}$'
  if [[ $1 =~ $re ]] && (( $1 / 100 >= 1 )) && (( $1 % 100 >= 1 )) && (( $1 % 100 <= 10)); then
    echo 0
  else
    echo 1
  fi
}

#"""
# practice
# ====================
#"""


chk_practice_rival() {
  # Args:
  #   $1  1~05
  local re='^0?[1-5]$'
  if [[ $1 =~ $re ]] && (( $1 >= 1 )) && (( $1 <= 5 )); then
    echo 0
  else
    echo 1
  fi
}


#"""
# Expedition
# ====================
#"""

chk_expd_id() {
  # Args:
  #   $1  NN  1~40, A1~A3, B1~B2
  # Returns:
  #   0: NN=1~40, A1~A3, B1~B2
  #   1: null or other

  local re_num='^[0-9]{1,3}$'
  local re_A='^A[1-3]$'
  local re_B='^B[1-2]$'

  # numeric id
  if [[ $1 =~ $re_num ]] && (( 10#$1 >= 1 )) && (( 10#$1 <= 40 )); then
    echo 0
  # text id
  elif [[ $1 =~ $re_A ]]; then
    echo 0
  elif [[ $1 =~ $re_B ]]; then
    echo 0
  else
    echo 1
  fi
}

chk_num() {
  # Args:
  #   $1  NN  1~40, A1~A3
  # Returns:
  #   0: NN=1~40
  #   1: other

  local re_num='^[0-9]{1,3}$'

  if [[ $1 =~ $re_num ]]; then
    echo 0
  else
    echo 1
  fi
}

chk_expd_fleet_id() {
  # Args:
  #   $1  2,3,4
  # Returns:
  #   0: 2,3,4
  #   1: null or other

  local re='^[2-4]$'
  if [[ $1 =~ $re ]]; then
    echo 0
  else
    echo 1
  fi
}


#"""
# Factory
# ====================
#"""

chk_develop_resource() {
  # Args:
  #   $1  fuel/ammo/steel/baux..
  # Returns:
  #   0: 10~300
  #   1: null or other

  local re="^[0-9]{2,3}$"
  if [[ $1 =~ $re ]] && (( $1 >= 10 )) && (( $1 <= 300 )); then
    echo 0
  else
    echo 1
  fi
}

chk_build_resource() {
  # Args:
  #   $1  fuel/ammo/steel/baux..
  # Returns:
  #   0: 30~300
  #   1: null or other

  local re="^[0-9]{2,3}$"
  if [[ $1 =~ $re ]] && (( $1 >= 30 )) && (( $1 <= 300 )); then
    echo 0
  else
    echo 1
  fi
}


#"""
# Sortie
# ====================
#"""

chk_seaarea() {
  # Args:
  #   $1  1~7  sea area
  # Returns:
  #   0: 1~7
  #   1: null or other

  local re="^[1-7]$"
  if [[ $1 =~ $re ]]; then
    echo 0
  else
    echo 1
  fi
}

chk_map() {
  # Args:
  #   $1  1~6  map
  # Returns:
  #   0: 1~6
  #   1: null or other

  local re="^[1-6]$"
  if [[ $1 =~ $re ]]; then
    echo 0
  else
    echo 1
  fi
}

chk_world() {
  # Args:
  #   $1  1~7  SeaArea
  #   $2  1~6  Map
  # Returns:
  #   0: 1-1~1-6, 2-1~2-5,.... 6-1~6-5
  #   1: null or other

  # NOTE: use [[ ]] to not fail when dealing with illegal input

  # SeaArea 1:  1-1~1-6
  if [[ $1 -eq 1 ]]; then
    if [[ $2 -ge 1 && $2 -le 6 ]]; then
      echo 0
    else
      echo 1
    fi
  # Sea Area 2,3,4,5,6: x-1~x-5
  elif [[ $1 -ge 1  &&  $1 -le 6 ]]; then
    if [[ $2 -ge 1  &&  $2 -le 5 ]]; then
      echo 0
    else
      echo 1
    fi
  # Sea Area 7: 7-1~7-2
  elif [[ $1 -eq 7 ]]; then
    if [[ $2 -ge 1 && $2 -le 2 ]]; then
      echo 0
    else
      echo 1
    fi
  else
    echo 1
  fi
}

chk_formation_single() {
  # Args:
  #   $1  1~6
  local re='^0?[1-6]$'
  if [[ $1 =~ $re ]] && (( $1 >= 1 )) && (( $1 <= 6 )); then
    echo 0
  else
    echo 1
  fi
}
