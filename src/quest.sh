#!/usr/bin/env bash
#"""
# Quest page actions
#"""

# dependencies
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/func.sh"


#"""
# Left-side Grouping Tabs
#"""

quest_all() {
  echo "$(date +%T) [Debug] quest_all()..."
  zmcs 18 70 127 12 1 $T_CLICK_API_ANIMATE
}

quest_undertaken() {
  echo "$(date +%T) [Debug] quest_undertaken()..."
  zmcs 18 70 156 12  1 $T_CLICK_API_ANIMATE
}

quest_daily() {
  echo "$(date +%T) [Debug] quest_daily()..."
  zmcs 18 70 186 12  1 $T_CLICK_API_ANIMATE
}

quest_weekly() {
  echo "$(date +%T) [Debug] quest_weekly()..."
  zmcs 18 70 215 12  1 $T_CLICK_API_ANIMATE
}

quest_monthly() {
  echo "$(date +%T) [Debug] quest_monthly()..."
  zmcs 18 70 244 12  1 $T_CLICK_API_ANIMATE
}

quest_once() {
  echo "$(date +%T) [Debug] quest_once()..."
  zmcs 18 70 273 12  1 $T_CLICK_API_ANIMATE
}

quest_others() {
  echo "$(date +%T) [Debug] quest_others()..."
  zmcs 18 70 303 12  1 $T_CLICK_API_ANIMATE
}

#"""
# Quest Pager
#"""


quest_page() {
  #"""
  # Select page
  #
  # Args:
  #   $1 (int): page 1~5 (won't have >=6 in the classification tabs)
  #"""

  echo "$(date +%T) [Debug] quest_page($@)..."
  zmcs $(( 295 + $1 * 53 )) 10 460 7  1 $T_CLICK_API_ANIMATE
}


quest_item() {
  #"""
  # Select an item
  #
  # Args:
  #   $1 (int): 1~5  which slot
  #"""
  echo "$(date +%T) [Debug] quest_item($@)..."
  zmcs 280 480 $(( 56 + $1 * 68 )) 40  1 $T_CLICK_API_ANIMATE
}


quest_complete() {
  #"""
  # Complete quest (wait/confirm)
  #
  # Args:
  #   $1 (int): times to click
  #"""
  echo "$(date +%T) [Info] quest_complete($@)..."

  if [ -z "$1" ]; then
    local N=1
  else
    local N=$1
  fi

  local i=1
  while (( i <= N )); do
    # use zmscs to prevent lag / slow animation
    zmscs 354 90 394 16 $T_CLICK_API_LONG 1 $T_CLICK_API_ANIMATE
    (( i += 1 ))
  done
}


quest_room() {
  #"""
  # Back to Teitoku Room
  #"""
  echo "$(date +%T) [Debug] quest_room()..."
  zmcs 14 86 446 28  1 $T_CLICK_API_LONG
}
