v1.2.0b5

Structure:
    * Refactored sleep durations into const.sh for more consistent
      parameter setting:
        - T_CLICK_INSTANT=612
        - T_CLICK_ANIMATE=987
        - T_CLICK_API=1822
        - T_CLICK_API_ANIMATE=2166
        - T_CLICK_API_LONG=3270

Misc:
    * expd_generic() logging
