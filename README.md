# kcautos1 v1.2.5 Documentation

# Table of contents

[1. About](#1-about)<br />
[2. Setup](#2-setup)<br />
[3. Customized Shell Environment](#3-customized-shell-environment)<br />
[4. User Scripts](#4-user-scripts)<br />
[# (1) Automated Expedition Cycle](#1-automated-expedition-cycle)<br />
[# (2) Kira (World 1-1 Morale Raise)](#2-kira-world-1-1-morale-raise)<br />
[# (3) Daily Quests](#3-daily-quests)<br />
[# (4) Non-stop Development](#4-non-stop-development)<br />
[# (5) Semi-automated Sortie](#5-semi-automated-sortie)<br />
[5. Development](#5-development)<br />
[# Action Functions](#action-functions)<br />
[# Click Pattern Randomization](#click-pattern-randomization)<br />
[TODO](#todo)<br />

# 1. About

**Disclaimer: Educational project. Use at your own risk!**

**A Python Pynput project will replace this soon.**

### What is this

The program uses `xdotool` in `bash` environment to manipulate Kancolle,
a popular browser game. This is linux-only.

Information from Kancolle Web API was NOT obtained or used by now. In other
words, simulated mouse click is the only way for this program to interact
with the game. This is significantly different from other "production
level" Web-API-based programs around the world. If you need one, simply
Google. This is only an "educational" one.

This program is the precursor of a planned future python version, which will
adapt to the upcoming HTML5 architecture switch of the game and utilize
(carefully) the Web API.


### Requirements

  * A Linux installation with a X11-based desktop environment
  * `bash`
    - `zsh`, `ksh`, `ash`.... **CAN'T** replace bash due to some
      bash-specific syntax.
  * `xdotool` package
    - CentOS 6/7 Users must also install [libxkbcommon-0.5.0-2.el7.x86_64](https://cbs.centos.org/koji/buildinfo?buildID=1409) manually.
  * (optional) Run on a [QEMU](https://www.qemu.org/)/[KVM](https://www.linux-kvm.org/page/Main_Page) virtual machine to free up your mouse!



### Screenshots

#### Automated Expedition Cycle

![](img/expedition-02-21-37.png)
<br />

#### One-click Development
![](img/development.png)
<br />

#### Automated 1-1 Sortie
![](img/sortie.png)
<br />


### Intro & Demo Videos (To be updated soon...)
  * [Installation & Brief Demo](https://www.facebook.com/realtemper/videos/vb.100000087454979/1498341130178788/?type=3&theater)


# 2. Setup

### Install xdotool

* Debian 8/9: `sudo apt-get install xdotool`

* CentOS 6/7:
  1) Download [libxkbcommon](https://cbs.centos.org/koji/buildinfo?buildID=1409) since EHEL didn't provide it.
      - [This solution](http://www.lampnode.com/linux/how-to-install-xdotool-on-centos) may not work in CentOS 7
  2) Install `libxkbcommon` by
    `sudo rpm -i libxkbcommon-0.5.0-2.el7.x86_64.rpm`
  3) `yum install -y xdotool`

### Program Setup

```bash
    git clone https://gitlab.com/cbhuang/kcautos1.git
    cd kcautos1
    ./setup.sh
    # Then follow the instructions.
    # DON'T do this: ./kcautos1/setup.sh
```

# 3. Customized Shell Environment

![custom-shell](img/custom-shell.png)

## Purpose
  * **For developer(s):** Gain direct access of action functions for
    convenience in testing.
  * **For users:** Same as above. Short user aliases. Good-looking.

## Usage

1. Enter

```bash
  cd script
  bash
  . custom-shell.sh <shipgirl>
    # <shipgirl> (optional) = murakumo (default), kasumi, isuzu, shikinami,
    # michishio, hatsukaze, akebono or ashigara. Just to give different
    # shell prompts (i.e. special variable PS1).
```

2. All action functions are now available. e.g.,

```bash
  叢雲：何をしているの？ ~/GitLab/kcautos1/script>
   $ develop 10 251 250 30 3
```

3. User scripts can also be executed.

```bash
  # same effect as above, plus safety questions
  叢雲：何をしているの？ ~/GitLab/kcautos1/script>
   $ ./develop.sh 10 251 250 30 3
```


# 4. User Scripts

### Overview

This section lists the most frequently used **user scripts**.

User scripts are `.sh` files under `script/`. They can be envoked under
system shell (e.g. `zsh`) or the custom shell environment.

* One must enter `script` directory before execution. i.e.:

    ```bash
      cd script
      ./script-to-run.sh arguments

      # DON'T do this: ./script/script-to-run.sh
    ```

* Most user scripts ask **safety questions** to help you get prepared!


### (1) Automated Expedition Cycle

#### Short-long-long (sll) Pattern

The pattern combines a short expedition with two longer ones. Denote the
times as `t_short`, `t_longer` and `t_longest`. Then the script works as
follows:

* The short expedition is re-sent before any long expd returns.
* The 3 fleets will be received together between `t_longest + t_short/3`
* The above together defines a "sll-cycle", the unit to be repeated.


The following example shows how to repeats expedition pattern (6, 37, 38)
for 3 cycles (9 hrs):


```bash
  # Way 1: in system or custom shell
  ./expd-sll.sh 6 37 38 3

  # Way 2: in custom shell
  esll 6 37 38 3

  # Way 3: in custom shell, default user alias
  ebs3   # "expedition bauxite & steel for 3 cycles."  
```


The script is designed to run regularly while you're at work. Unlike the
next generic script which re-sends every fleet immediately, this script
"wastes" some time in order to synchronize fleet return. This resembles
human behavior more when `(5, 37, 38)` or `(21, 37, 38)` is chosen.


#### Generic (Nonstop) Pattern

This expedition pattern resends each fleet immediately and is therefore
applicable to **ANY combination of expeditions**.

The following example shows how to send (5, 21, 38) within 11h40m.
Note: This completes 5 times of expd 21, 4 cycles of expd 38, and 8 times
of expd 5 which will return at approximately 12h00m.


```bash
  # Way 1: in system or custom shell
  ./expd-generic.sh 5 21 38 700
  # or if you will assign ships to sortie 1-1 (twice each)
  ./expd-generic.sh 5 21 38 700 y

  # Way 2: in custom shell
  egen 5 21 38 700
  # assign ships to sortie 1-1
  egen 5 21 38 700 y

  # Of course you can define your alias in alias.sh
```

**WARNING: This script is NOT intended for regular use** because the time
usage is too perfect to be human-like. It's only helpful when you're stuck
in an event and need every drop of resource. **Use at your very own risk!**


##### Notes on the scheduler algo

The subtle part of the algorithm is that when multiple fleets returns at
once, subsequent schedule will be interrupted. This is because > 60s are
required to complete the re-send procedure, and that exceeds the error
tolerance (60s) of early receive. Consequently, I take measures as follows:

  * Make a naive schedule of returning time and fleet(s)
  * Start scanning from the first return:
    - If 2 fleets returns together, delay 20s for all later schedule
    - If 3 fleets returns together, delay 50s for all later schedule

In all practical cases, these delays will merely cause earlier-sent
expeditions to be received some 20s or 40s later than normal. Since
expedition durations are differed by a minimum unit of 5 minutes, the
delayed schedule, practically, will not collide with expeditions that were
sent earlier unless you do something silly like (1=15m, 2=30m, 32=24h) for
several cycles that ramps up a large amount of delay.

The details can be found in `expd_generic()` within `action.sh`.


### (2) Kira (World 1-1 Morale Raise)

#### Massive Kira

* Do this when you are asleep or at work.
  * Better also undertake A-Gou Sakusen at Monday night!
* You have chance to edit `mass-kira.txt` and see ship enumeration rules.
* e.g. Sortie 1-1 twice for each ship

```bash
  # Way 1: in system or custom shell
  ./mass-kira.sh 2

  # Way 2: in custom shell, default user alias
  mk 2
```

#### Flagship Only

* e.g. Sortie 3 times

```bash
  # Way 1: in system or custom shell
  ./sortie-1-1.sh 3

  # Way 2: in custom shell, default user alias
  s11 3
```

### (3) Daily Quests

#### Factory Quest

* Do all daily factory quests except equipment improvement.

```bash
  # Way 1: in system or custom shell
  # Args: = development recipe
  ./quest-daily-factory.sh 10 251 250 30

  # Way 2: in custom shell, default user alias
  qdf 10 251 250 30
```

#### Sortie Quest

* Do the 3 daily sortie quests (1+1+10 combats).

```bash
  # Way 1: in system or custom shell
  # Args:
  # $1     y/n   undertake A-Gou Sakusen or not
  # $2~$5  XXYY  4 ships to complete the quests. See
  #              mass-kira.txt for enumeration rules
  ./quest-daily-sortie.sh n 210 1602 1605 9902

  # Way 2: in custom shell, default user alias
  qds n 210 1602 1605 9902
```

#### All-in-one

```bash
  # Way 1: in system or custom shell
  ./quest-daily-fs.sh 10 251 250 30 n 210 1602 1605 9902

  # Way 2: in custom shell, default user alias
  qdfs 10 251 250 30 n 210 1602 1605 9902
```

### (4) Non-stop Development

* All-10 for 87 times non-stop
  * make sure you have enough vacancy in equipments

```bash
  # Way 1: in system or custom shell
  ./develop.sh 10 10 10 10 87

  # Way 2: in custom shell
  develop 10 10 10 10 87
```

### (5) Semi-automated Sortie

* **Purpose: Automate predictable clicks EXCEPT ADVANCEMENT.**
* **WARNING: There's no heavy damage detection. Use at your own risk!**
* Demo videos (to be updated soon....)

```bash
  # Way 1: in system or custom shell
  # specific world, pass parameters if any
  ./sortie-5-2-C.sh 10
  # generic (World M-N)
  ./sortie.sh M N

  # Way 2: in custom shell, default user alias
  # World 5-2 (MRY train)
  s52c
  # generic (World M-N)
  sortie M N
```


# 5. Development

## Action Functions

### Basic Unit

An action is defined as a basic unit of **"move-click-sleep (mcs)"**:

1. **Move** mouse to a point within an assigned area
2. **Click** the button
3. **Sleep** for a period

The function `mcs()` is implemented in `src/func.sh`. There are also
occasional variants around: `ms()` (move-sleep) and `mscs()`
(move-sleep-click-sleep).

### Usage

All actions are coded via the above basic action units' back-end partner
functions, i.e. `zmcs()`, `zms()` and `zmscs()`.

The `"z-"` functions adapts to the **zooming and positioning** of the game
window. The parameters of `z-` functions are *normalized to 100% zoom ratio
relative to the top-left corner of the game window*.

The next section explains how the mouse coordinates are obtained.

### Capture Location Parameters

1. Envoke the capturing script:

```bash
    # Way 1
    cd utils
    ./getmouselocation.sh

    # Way 2, when you are under script/, src/ or test/
    ../utils/getmouselocation.sh    
```

2. Follow script instructions and move your mouse accordingly.
3. Copy obtained location parameters like this:

    ![](img/getmouselocation.png)

<br />

## Click Pattern Randomization

**Both mouse locations and click durations are randomized** in order to
avoid *hypothetically* existing space- or time- pattern detection programs
from Tanaka. (Yeah this program is educational, so it is not important
  whether the detection mechanism exists.)

The mechanism is described below:

1. **Location**
    - In `z-` functions, the location to be clicked is always defined by a
      **rectangular area**.
    - That's why we need `getmouselocation.sh` to produce 4 location
      parameters instead of 2.


2. **Time**
    - The minimal sleep time is passed into a `z-` function.
    - Subsequently, it is randomized by `randsleepms()`
    - If the global variable `DBG_RNDTIME=y` is set in `conf/prj.conf`,
      `randsleepms()` produces random "inefficient" lags to simulate
      delays in the real world.
    - Finally, the randomized time is converted into microseconds and then
      fed into self-compiled `usleep.exe`.


# TODO

- Add extra delay mode (`$T_HOLD` in `const.sh`) setup script to custom to
  new data loads. (will eventually replaced by API listening)


No major changes will be made to the current version (1.0) due to an
announced HTML5 reform of the game in Spring 2018 (Done in Aug. 17, 2018).
However, there are some features planned for the future python version:

* Use information from Web API (carefully):
  - **Test network connection** before making crucial clicks.
  - Fully-automated ship selection/composition.
  - Fully-automated equipment selection (for expeditions).
  - Fully-automated sortie, retreat, supply and repair.
  - May consider integration with [poi](https://github.com/poooi/poi).


* Cross-Platform Availability
  - Switch to Python 3:
    - `PyUserInput` library for mouse clicks
    - `urllib2/3` for accessing web-API
  - Docker deployment (depends)


* Anti-anti-script measure
  - More sophisticated randomization techniques
  - Emulated input device [information for judging script-generated clicks in javascript](https://stackoverflow.com/questions/6674669/in-jquery-how-can-i-tell-between-a-programmatic-and-user-click)
