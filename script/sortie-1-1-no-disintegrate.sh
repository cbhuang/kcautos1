#!/usr/bin/env bash
#"""
# World 1-1 single ship sortie - no disintegration
#
# Args:
#     $1 (int): sortie count
#"""

# project settings
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"

# action library
source "${SRCHOME}/action.sh"


#"""
# check
#"""

# command args
if [ -z "$1" ]; then
  echo "$(date +%T) [Debug] Bad sortie count argument, set to 1"
  arg1=1
else
  arg1=$1
fi

# safety questions
declare -a qs=(
  "Ship vacancy >= $((2*arg1))?"
  "Expedition: no fleet return within $((4*arg1)) mins!"
  "All done?"
)
question_ask qs[@]


#"""
# Main Program
#"""

echo "$(date +%T) [Info] sortie-1-1-no-disintegrate.sh $@ begins..."
countdown 3
sortie_1_1_no_disintegrate $arg1
echo "$(date +%T) [Info] sortie-1-1-no-disintegrate.sh $@ done!"
