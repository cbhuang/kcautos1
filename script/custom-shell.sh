#!/usr/bin/env bash
#"""
# Custom shell environment
#
# Args:
#     $1 (str): shipname (default=murakumo)
#
# Usage:
#
# .. code-block:: bash
#
#    # start a subshell (the console won't close when exit)
#    bash
#    # load custom environment
#    . custom-shell.sh $1
#"""

custom_prompt() {
  #"""
  # Args:
  #   $1 (str): shipname
  #"""

  local FLAGSHIP
  local TXT_GREET
  local TXT_PROMPT

  case "$1" in
    "ashigara")
      FLAGSHIP="足柄"
      TXT_GREET="私の能力を本当にちゃんと引き出せたのは…あなたが…そう、あなたが初めてよ！"
      TXT_PROMPT="なんですか？出撃ですか？"
      ;;
    "michishio")
      FLAGSHIP="満潮"
      TXT_GREET="満潮よ。私、なんでこんな部隊に配属されたのかしら？"
      TXT_PROMPT="…うるさいわね。"
      ;;
    "shikinami")
      FLAGSHIP="敷波"
      TXT_GREET="用がないならいちいち呼び出さないでよー。…別に、い、嫌じゃないけどさ。"
      TXT_PROMPT="お、呼んだ？"
      ;;
    "akebono")
      FLAGSHIP="曙"
      TXT_GREET="特型駆逐艦『曙』よ。って、こっち見んな！このクソ提督！"
      TXT_PROMPT="このクソ提督！"
      ;;
    "hatsukaze")
      FLAGSHIP="初風"
      TXT_GREET="ちょっ！何触ってんのよ！ぶつわよ！たたくわよ！妙高姉さんに言いつけるわよ！"
      TXT_PROMPT="ご飯、まだなの？"
      ;;
    "murakumo")
      FLAGSHIP="叢雲"
      TXT_GREET="あんたが司令官ね。ま、せいぜい頑張りなさい！"
      TXT_PROMPT="何をしているの？"
      ;;
    "kasumi")
      FLAGSHIP="霞"
      TXT_GREET="霞、出るわ。見てらんないったら！"
      TXT_PROMPT="だから何よ？"
      ;;
    "isuzu")
      FLAGSHIP="五十鈴"
      TXT_GREET="バカね、撃ってくれってこと？"
      TXT_PROMPT="五十鈴に御用？"
      ;;
    "tanaka")
      FLAGSHIP="田中"
      TXT_GREET="17夏活吃屎爽嗎？"
      TXT_PROMPT="你要享受這個過程！"
      ;;
    *)
      FLAGSHIP="叢雲"
      TXT_GREET="あんたが司令官ね。ま、せいぜい頑張りなさい！"
      TXT_PROMPT="何をしているの？"
      ;;
  esac

  # change prompt
  echo -e "\n  ${FLAGSHIP}：All functions were loaded via ../src/action.sh !\n"
  echo -e "  ${FLAGSHIP}：「${TXT_GREET}」\n"

  PS1="\[\e[01;33m\]${FLAGSHIP}：${TXT_PROMPT}\[\e[00m\] \w>\n \\$ "
}

# load enviroment
source "../src/action.sh"
source "${SCRIPTHOME}/alias.sh"
# change prompt
custom_prompt "$1"
