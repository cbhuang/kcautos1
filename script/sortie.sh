#!/usr/bin/env bash
#"""
# Generic semi-automated interactive sortie script.
# Wrapper for sortie() in action.sh.
#
# Args:
#   $1 (int): 1~6  seaarea
#   $2 (int): 1~6  map
#"""

# project settings
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"

# actions
source "${SRCHOME}/action.sh"

#"""
# Check
#"""

# check args
if [ $(chk_world $1 $2) -eq 1 ]; then
  echo "$(date $T) [Error] Bad world: $@"
  exit 1
fi

# safety questions
declare -a qs=(
  "Supplied"
  "Sidebar is visible"
  "All done? (NOTE: NO supply & disintegration after fleet return!)"
)
question_ask qs[@]


#"""
# Main Program
#"""

# send
echo "$(date +%T) [Info] sortie.sh $@ begins..."
# countdown in sortie()
sortie $1 $2
echo "$(date +%T) [Info] sortie.sh $@ done!"
