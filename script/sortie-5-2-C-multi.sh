#!/usr/bin/env bash
#"""
# Raise MRY to Lv.20 using 5-2-C, multiple count
#
# Args:
#    $1~$3  ship,lv,exp   shipno, current lv, exp to next lv
#    ....
#"""

# project config
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"

# actions
source "${SRCHOME}/action.sh"

# check argument count
if (( $# % 3 != 0 )); then
  echo "$(date +%T) [Error] Bad parameter count: $#"
  exit 1
fi

# safety questions
declare -a qs=(
  "Formation: Is flagship irrelevant?"
  "Is sidebar visible?"
  "All done?"
)
question_ask qs[@]


#"""
# Main
#"""

echo "$(date +%T) [Info] sortie_5_2_C_to20_multi($@) begins..."
countdown 3

i=1
while (( i <= $# )); do
  sortie_5_2_C_to20 ${@:i:3}
  (( i += 3 ))
done

echo "$(date +%T) [Info] sortie_5_2_C_to20_multi($@) done!"
