#!/usr/bin/env bash
#"""
# Wrapper of expd_generic(5 21 38 700)
#"""

# project settings
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"

# actions
source "${SRCHOME}/action.sh"


#"""
# check
#"""

# security questions
declare -a qs=(
  "Is sidebar visible?"
  "Composition for expd (5, 21, 38)?"
  "Morale >= (71, 62, 59)?"
  "Equipment: Doramu Cans & Vessels?"
  "All done?"
)
question_ask qs[@]

#"""
# Main
#"""

echo "$(date +%T) [Info] expd-generic(5 21 38 700) begins..."
countdown 3
expd_generic 5 21 38 700
echo "$(date +%T) [Info] expd-generic(5 21 38 700) done!"
