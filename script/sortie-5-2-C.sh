#!/usr/bin/env bash
#"""
# World 5-2-C full-auto sortie script
#
# Args:
#   $1  1..  how many times
#"""

# project config
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"

# actions
source "${SRCHOME}/action.sh"

# command args
if [ -z "$1" ] || [ $(chk_num $1) -eq 1 ]; then
  echo "$(date +%T) [Debug] Bad sortie count, set to 1"
  arg1=1
else
  arg1=$1
fi

# safety questions
declare -a qs=(
  "Formation: SS only?"
  "Is sidebar visible?"
  "All done?"
)
question_ask qs[@]


#"""
# Main
#"""

echo "$(date +%T) [Info] sortie-5-2-C.sh $1 begins...."
countdown 3
sortie_5_2_C $arg1
echo "$(date +%T) [Info] sortie-5-2-C.sh $1 done!"
