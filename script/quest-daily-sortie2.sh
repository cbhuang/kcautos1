#!/usr/bin/env bash
#"""
# Do the first 2 daily sortie quests.
# Flagship must be manually arranged.
#"""

# project settings
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"

# action APIs
source "${SRCHOME}/action.sh"

# command args are checked within quest_daily_sortie()

# safety questions
declare -a qs=(
  "Quest button is visible"
  "Quest vacancy >= 1"
  "Formation: Ship to sortie is set"
  "Disintegration: vacancy: ship >= 2 & equipment >= 4"
  "Disintegration: order by new & top ship locked"
  "Expedition: no fleet return within 12 mins!"
  "All done?"
)
question_ask qs[@]

#"""
# Main Program
#"""
echo "$(date +%T) [Info] quest-daily-sortie.sh $@ begins..."
countdown 3
quest_daily_sortie2
echo "$(date +%T) [Info] quest-daily-sortie.sh $@ done!"
