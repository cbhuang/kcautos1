#!/usr/bin/env bash
#"""
# Do daily factory & sortie quests consecutively
#
# Args:
#    $1~$4 (int):  10~300  development recipe
#    $5 (char):    y/n     undertake A-Gou or not
#    $6~$9 (int):  XXYY    4 ships to sortie
#"""

# project settings
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"

# action APIs
source "${SRCHOME}/action.sh"

#"""
# check
#"""

# Argument count
if [ -z "$9" ]; then
  echo "$(date +%T) [Error] Bad development recipe!"
  exit 1
fi

# safety questions
declare -a qs=(
  "Resource >= ($(($1*4 - 60)), $(($2*4 - 60)), $(($3*4 - 60)), $(($4*4 - 60)))"
  "Quests were untouched / won't affect slot position"
  "Quest button is visible"
  "Quest vacancy >= 2 or 3(when undertaking A-Gou Sakusen)"
  "Formation: flagship for development"
  "Disintegration: vacancy: ship >= 3 & equipment >= 8"
  "Disintegration: order by new & top ship locked"
  "Expedition: no fleet return within 37 mins!"
  "All done?"
)
question_ask qs[@]

#"""
# Main Program
#"""
echo "$(date +%T) [Info] quest-daily-fs.sh $@ begins..."
countdown 3
quest_daily_factory $1 $2 $3 $4
quest_daily_sortie $5 $6 $7 $8 $9
echo "$(date +%T) [Info] quest-daily-fs.sh $@ done!"
