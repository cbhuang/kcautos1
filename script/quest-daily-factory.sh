#!/usr/bin/env bash
#"""
# Do daily factory quests (except improvement)
#
# Args:
#    $1..$4: development recipe
#"""

# project settings
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"

# action library
source "${SRCHOME}/action.sh"

#"""
# check
#"""

# argument count
if [ -z "$4" ]; then
  echo "$(date +%T) [Error] Bad development recipe!"
  exit 1
fi

# safety questions
declare -a qs=(
  "Quest: '10 combats daily' is NOT DONE"
  "Quest button is visible"
  "Resources >= ($(($1*4 - 60)), $(($2*4 - 60)), $(($3*4 - 60)), $(($4*4 - 60)))"
  "Quest vacancy >= 2"
  "Formation: Correct flagship type for development"
  "Factory: All construction slots are empty"
  "Disintegration: vacancy: ship >= 3 & equipment >= 8"
  "Disintegration: order by new & top ship locked"
  "Expedition: no fleet return within 8 mins!"
  "All done?"
)
question_ask qs[@]


#"""
# Main
#"""

echo "$(date +%T) [Info] quest-daily-factory.sh $@ begins..."
countdown 3
quest_daily_factory $1 $2 $3 $4
echo "$(date +%T) [Info] quest-daily-factory.sh $@ done!"
