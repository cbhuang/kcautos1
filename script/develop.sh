#!/usr/bin/env bash
#"""
# User Script of develop()
#
# Args:
#   $1~$4  10~300  recipe
#   $5     >=1     repeat N times
#"""

# project settings
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"

# actions (do not need the others)
source "${SRCHOME}/factory.sh"

# main program
echo "$(date +%T) [Info] develop.sh($@) begins..."
countdown 3
develop $1 $2 $3 $4 $5
echo "$(date +%T) [Info] develop.sh($@) done!"
