#!/usr/bin/env bash
#"""
# World 5-3-P semi-auto sortie script
#
# Assumed route: D->G->I->O->P->retreat
#"""

# project config
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"

# actions
source "${SRCHOME}/action.sh"

# Warning (5-3 drops are good for modernization)
echo "$(date +%T) [Info] Note: NO disintegration after fleet return!"

# safety questions
declare -a qs=(
  "Correct formation?"
  "Supplied?"
  "Is sidebar visible?"
  "All done?"
)
question_ask qs[@]


#"""
# Main
#"""

echo "$(date +%T) [Info] sortie-5-3-P.sh begins...."
countdown 3
sortie_5_3_P
echo "$(date +%T) [Info] sortie-5-3-P.sh done!"
