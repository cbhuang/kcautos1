#!/usr/bin/env bash
#"""
# World 6-5-B startup script
#"""

# project config
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"

# actions
source "${SRCHOME}/action.sh"

# Warning (5-3 drops are good for modernization)
echo "$(date +%T) [Info] Note: NO disintegration after fleet return!"

# safety questions
declare -a qs=(
  "Correct bottom route formation? (Nelson Shot recommended)"
  "Is LBAS deployed?"
  "Is sidebar visible?"
  "All done?"
)
question_ask qs[@]


#"""
# Main
#"""

echo "$(date +%T) [Info] sortie-6-5-B.sh begins...."
countdown 3
sortie_6_5_B_first
echo "$(date +%T) [Info] sortie-6-5-B.sh done!"
