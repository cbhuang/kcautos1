#!/usr/bin/env bash
#"""
# Repetitive Expedition Patterns (short, long, long)
#
# Args:
#   $1~$3 (expd_id): expedition ids (numeric or letter-num)
#   $4 (int): how many cycles
#"""

# project settings
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"

# actions
source "${SRCHOME}/action.sh"


#"""
# check
#"""

# check expd id
if [ $(chk_expd_id $1) -eq 1 ] || [ $(chk_expd_id $2) -eq 1 ] || \
   [ $(chk_expd_id $3) -eq 1 ]; then
  echo "$(date +%T) [Error] Bad expd ids: $1 $2 $3"
  exit 1
fi

# check #cycles
if [ -z "$4" ]; then
  echo "$(date +%T) [Info] Cycles not given, set to 1"
  N=1
elif (( $4 >= 1 )); then
  N=$4
else
  echo "$(date +%T) [Error] Bad #Times: $4"
  exit 1
fi

# security questions
declare -a qs=(
  "Is sidebar visible"
  "Hensei: Composition & Morale >= $((47 + N*3))?"
  "Equipment: Doramu Cans & Vessels?"
  "All done?"
)
question_ask qs[@]

#"""
# Main
#"""

echo "$(date +%T) [Info] expd-sll.sh begins..."
countdown 3
expd_cycle_sll $1 $2 $3 $N
echo "$(date +%T) [Info] expd-sll.sh done!"
