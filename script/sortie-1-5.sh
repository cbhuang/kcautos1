#!/usr/bin/env bash
#"""
# World 1-5 semi-auto sortie script
#"""

# project config
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"

# action library
source "${SRCHOME}/action.sh"


# safety questions
declare -a qs=(
  "Supplied"
  "Sidebar is visible"
  "All done? (NOTE: NO supply & disintegration after fleet return!)"
)
question_ask qs[@]

#"""
# Main
#"""

echo "$(date +%T) [Info] sortie-1-5.sh begins...."
countdown 3
sortie_1_5
echo "$(date +%T) [Info] sortie-1-5.sh done!"
