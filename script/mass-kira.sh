#!/usr/bin/env bash
#"""
# Massive kirakira for the assigned ships
#
# Args:
#     $1  1~3  times to sortie per ship (default=2)
#"""

# project settings
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"

# actions
source "${SRCHOME}/action.sh"

# command args
if [ -z "$1" ]; then
  echo "$(date +%T) [Debug] No sortie count. Set to 2"
  n_sortie=2
elif (( $1 >= 1 )) && (( $1 <= 3 )); then
  n_sortie=$1
else
  echo "$(date +%T) [Debug] Bad sortie count: $1"
  exit 1
fi


#"""
# User Input
#"""

edit_shipfile
ships=$(read_shipfile)
echo "$(date +%T) [Debug] Ship list read: $ships"

# Safety Reminders
declare -a qs=(
  "Is sidebar visible?"
  "Formation: 1st fleet has only 1 irrelevant ship"
  "Resources >= (16*#ships to sortie)"
  "Disintegration: vacancy: ship >= 2 & equipment >= 4"
  "Disintegration: order by new & top ship locked"
  "Expedition: No return during sorties"
  "All done?"
)
question_ask qs[@]


#"""
# Main
#"""

echo "$(date +%T) [Info] mass-kira.sh $1 begins...."
countdown 3

for ship in $ships; do

  # sidebar should be visible

  # select ship
  echo "$(date +%T) [Info] Send ship $ship ..."
  sidebar_hensei
  hensei_henkou 1 $ship

  # sortie (includes supply)
  sortie_1_1_disintegrate4 $n_sortie
  echo "$(date +%T) [Info] ship $ship done!"
done

echo "$(date +%T) [Info] mass-kira.sh $1 ended successully!"
