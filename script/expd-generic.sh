#!/usr/bin/env bash
#"""
# Wrapper of expd_generic.sh
#
# Args:
#   $1~$3 (expd_id)
#   $4 (int): duration (minutes)
#   $5 (y/n): sortie_1_1 while wait
#"""

# project settings
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"

# actions
source "${SRCHOME}/action.sh"


#"""
# check
#"""

# check expd id
if [ $(chk_expd_id $1) -eq 1 ] || [ $(chk_expd_id $2) -eq 1 ] || \
   [ $(chk_expd_id $3) -eq 1 ]; then
  echo "$(date +%T) [Error] Bad expd ids: $1 $2 $3"
  exit 1
fi

# check time
if [ -z "$4" ]; then
  echo "$(date +%T) [Error] Duration not given!"
  exit 1
fi

# security questions
declare -a qs=(
  "Is sidebar visible?"
  "Hensei: Composition & Morale?"
  "Equipment: Doramu Cans & Vessels?"
  "All done?"
)
question_ask qs[@]

#"""
# Main
#"""

echo "$(date +%T) [Info] expd-generic.sh($@) begins..."
# countdown if there's no need to edit ships later
if [ $(chk_yes $5) -ne 0 ]; then
  countdown 3
fi
expd_generic $@
echo "$(date +%T) [Info] expd-generic.sh($@) done!"
