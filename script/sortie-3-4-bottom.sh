#!/usr/bin/env bash
#"""
# World 3-4 bottom route semi-auto sortie script
# Assumed formation: 2CL+3DD+1CA(V)
#"""

# project config
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"

# actions
source "${SRCHOME}/action.sh"

# safety questions
declare -a qs=(
  "Formation: 2CL+3DD+1CA(V)?"
  "Disintegration: vacancy - ship >= 2 && equipment >= 4 ?"
  "Is sidebar visible?"
  "All done?"
)
question_ask qs[@]


#"""
# Main
#"""

echo "$(date +%T) [Info] sortie-3-4-bottom.sh begins...."
countdown 3
sortie_3_4_bottom
echo "$(date +%T) [Info] sortie-3-4-bottom.sh done!"
