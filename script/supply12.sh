#!/usr/bin/env bash
#"""
# Auto supply 12 times (wrapper for supply12()).
#"""

# project settings
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"

# actions
source "${SRCHOME}/supply.sh"

#"""
# Main Program
#"""
echo "$(date +%T) [Info] supply12.sh $@ begins...."
countdown 3
supply12
echo "$(date +%T) [Info] supply12.sh $@ done!"
