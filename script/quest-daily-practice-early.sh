#!/usr/bin/env bash
#"""
# Do the daily practice quest (early half)
#
# Args:
#    $1      y/n  process quest or not
#    $2,$3        rival slot, formation
#    $4,$5        rival slot, formation
#    $6,$7        rival slot, formation
#    $8,$9        rival slot, formation
#    $10,$11      rival slot, formation
#"""

# project settings
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"

# action APIs
source "${SRCHOME}/action.sh"

# command args are checked within quest_daily_sortie()

# safety questions
declare -a qs=(
  "Arguments: y/n, (slot, formation)*5"
  "Quest button is visible"
  "Quest: daily 3AO is not completed"
  "Quest: vacancy >= 1"
  "Formation: 6 ships"
  "Expedition: no fleet return within ~30 mins!"
  "All done?"
)
question_ask qs[@]

#"""
# Main Program
#"""
echo "$(date +%T) [Info] quest-daily-practice-early.sh $@ begins..."
countdown 3
quest_daily_practice_early $@ 
echo "$(date +%T) [Info] quest-daily-practice-early.sh $@ done!"
