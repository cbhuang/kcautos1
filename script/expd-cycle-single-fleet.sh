#!/usr/bin/env bash
#"""
# Repetitive Expedition for Single Fleet
#
# :date: 2017-10-22
# :author: Chuan-Bin "Bill" Huang
# :contact: cbhuang@ntu.edu.tw
#
# Args:
#   $1 (fleet_id): 2~4
#   $2 (expd_id): expd ids (numeric or letter-num)
#   $3 (int): cycle count
#
# Example:
#
#     # (within custum shell environment)
#     # send fleet 2 to expd A1 for 6 times
#     $ ecsf 2 A1 6
#
# Todo:
#     * Use doxpybash/basphinx document generator
#     * Check "Google Python Style Guide <http://google.github.io/styleguide/pyguide.html>"_
#"""

# project settings
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"

# actions
source "${SRCHOME}/action.sh"


#"""
# check
#"""

# fleet_id
if [ $(chk_fleet_id $1) -eq 1 ]; then
  echo "$(date +%T) [Error] Bad fleet_id: $1"
  exit 1
fi

# expd_id
if [ $(chk_expd_id $2) -eq 1 ]; then
  echo "$(date +%T) [Error] Bad expd_id: $1 $2 $3"
  exit 1
fi

# check cycle count
if ! (( $3 > 0 )); then
  echo "$(date +%T) [Error] Bad cycle count: $3"
  exit 1
fi

# confirmation
read_safe res "Send fleet $1 to expd $2 for $3 times? (y/n)"
if [ $(chk_yes $res) -eq 1 ]; then
  echo "User abort."
  exit 1
fi


#"""
# Main
#"""

echo "$(date +%T) [Info] expd-cycle-single-fleet.sh $@ begins..."
countdown 3
expd_cycle_single_fleet $1 $2 $3
echo "$(date +%T) [Info] expd-cycle-single-fleet.sh $@ done!"
