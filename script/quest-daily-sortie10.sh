#!/usr/bin/env bash
#"""
#  Do the 10-sorties daily quest.
#  Ship 1,2 sorties twice. The last sorties once.
#
# Args:
#    $1     y/n   undertake A-Gou Sakusen
#    $2~$4  XXYY  ship No. for 10 combats
#"""

# project settings
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"

# action APIs
source "${SRCHOME}/action.sh"

# command args are checked within quest_daily_sortie()

# safety questions
declare -a qs=(
  "Arguments: y/n, XXYY*4"
  "Quest button is visible"
  "Quest vacancy >= 1 or 2(when undertaking A-Gou Sakusen)"
  "Formation: flagship is an irrelavant one"
  "Disintegration: vacancy: ship >= 2 & equipment >= 4"
  "Disintegration: order by new & top ship locked"
  "Expedition: no fleet return within 25 mins!"
  "All done?"
)
question_ask qs[@]

#"""
# Main Program
#"""
echo "$(date +%T) [Info] quest-daily-sortie10.sh $@ begins..."
countdown 3
quest_daily_sortie10 $1 $2 $3 $4
echo "$(date +%T) [Info] quest-daily-sortie10.sh $@ done!"
