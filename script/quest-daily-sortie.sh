#!/usr/bin/env bash
#"""
# Do daily sortie quests.
# Ship 1~3 will sortie World 1-1 twice. Ship4 once.
#
# Args:
#    $1 (char):   y/n   undertake A-Gou Sakusen
#    $2 (int):    XXYY  ship No. for 1+1 sortie
#    $3~$5 (int): XXYY  ship No. for 10 combats
#"""

# project settings
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"

# action APIs
source "${SRCHOME}/action.sh"

# command args are checked within quest_daily_sortie()

# safety questions
declare -a qs=(
  "Arguments: y/n, XXYY*4"
  "Quest button is visible"
  "Quest vacancy >= 1 or 2(when undertaking A-Gou Sakusen)"
  "Formation: flagship is an irrelavant one"
  "Disintegration: vacancy: ship >= 3 & equipment >= 8"
  "Disintegration: order by new & top ship locked"
  "Expedition: no fleet return within 38 mins!"
  "All done?"
)
question_ask qs[@]

#"""
# Main Program
#"""
echo "$(date +%T) [Info] quest-daily-sortie.sh $@ begins..."
countdown 3
quest_daily_sortie $1 $2 $3 $4 $5
echo "$(date +%T) [Info] quest-daily-sortie.sh $@ done!"
