#!/usr/bin/env bats
#"""
# Test chkvar.sh
#
# Require `BATS test framework <https://github.com/sstephenson/bats>`_
#"""

setup() {
  source "../src/chkvar.sh"
}

###############
# generic
###############

@test "chk_yes" {

  [ $(chk_yes y) -eq 0 ]
  [ $(chk_yes Y) -eq 0 ]
  [ $(chk_yes "y") -eq 0 ]

  [ $(chk_yes ) -eq 1 ]
  [ $(chk_yes n) -eq 1 ]
  [ $(chk_yes N) -eq 1 ]
  [ $(chk_yes "yy") -eq 1 ]
  [ $(chk_yes 0y) -eq 1 ]
}


@test "chk_no" {

  [ $(chk_no n) -eq 0 ]
  [ $(chk_no N) -eq 0 ]
  [ $(chk_no "n") -eq 0 ]

  [ $(chk_no ) -eq 1 ]
  [ $(chk_no y) -eq 1 ]
  [ $(chk_no Y) -eq 1 ]
  [ $(chk_no "nn") -eq 1 ]
  [ $(chk_no 0y) -eq 1 ]
}


@test "chk_num" {

  [ $(chk_num 1) -eq 0 ]
  [ $(chk_num 01) -eq 0 ]
  [ $(chk_num "01") -eq 0 ]

  [ $(chk_num A1) -eq 1 ]
  [ $(chk_num "A1") -eq 1 ]
  [ $(chk_num a3) -eq 1 ]
}


@test "chk_ltnum" {

  [ $(chk_ltnum A00) -eq 0 ]
  [ $(chk_ltnum A9) -eq 0 ]
  [ $(chk_ltnum A09) -eq 0 ]
  [ $(chk_ltnum A099) -eq 0 ]
  [ $(chk_ltnum Z999) -eq 0 ]

  [ $(chk_ltnum 000) -eq 1 ]
  [ $(chk_ltnum 1) -eq 1 ]
  [ $(chk_ltnum 09) -eq 1 ]
  [ $(chk_ltnum 999) -eq 1 ]
  [ $(chk_ltnum A) -eq 1 ]
  [ $(chk_ltnum a9) -eq 1 ]
  [ $(chk_ltnum Z1000) -eq 1 ]
}


@test "chk_genid" {

  [ $(chk_genid 000) -eq 0 ]
  [ $(chk_genid 1) -eq 0 ]
  [ $(chk_genid 01) -eq 0 ]
  [ $(chk_genid 001) -eq 0 ]
  [ $(chk_genid 09) -eq 0 ]
  [ $(chk_genid 099) -eq 0 ]
  [ $(chk_genid 999) -eq 0 ]
  [ $(chk_genid A00) -eq 0 ]
  [ $(chk_genid A9) -eq 0 ]
  [ $(chk_genid A09) -eq 0 ]
  [ $(chk_genid A099) -eq 0 ]
  [ $(chk_genid Z999) -eq 0 ]

  [ $(chk_genid A) -eq 1 ]
  [ $(chk_genid a9) -eq 1 ]
  [ $(chk_genid Z1000) -eq 1 ]
}


@test "chk_fleet_id" {

  [ $(chk_fleet_id 1) -eq 0 ]
  [ $(chk_fleet_id 4) -eq 0 ]

  [ $(chk_fleet_id ) -eq 1 ]
  [ $(chk_fleet_id 0) -eq 1 ]
  [ $(chk_fleet_id 01) -eq 1 ]
  [ $(chk_fleet_id 5) -eq 1 ]
  [ $(chk_fleet_id 10) -eq 1 ]
  [ $(chk_fleet_id -1) -eq 1 ]
}


###############
# Practice
###############

@test "chk_practice_rival" {

  [ $(chk_practice_rival 1) -eq 0 ]
  [ $(chk_practice_rival 01) -eq 0 ]
  [ $(chk_practice_rival "01") -eq 0 ]
  [ $(chk_practice_rival 5) -eq 0 ]

  [ $(chk_practice_rival ) -eq 1 ]
  [ $(chk_practice_rival 0) -eq 1 ]
  [ $(chk_practice_rival 6) -eq 1 ]
}


###############
# Factory
###############

@test "chk_develop_resource" {

  [ $(chk_develop_resource 10) -eq 0 ]
  [ $(chk_develop_resource 300) -eq 0 ]

  [ $(chk_develop_resource ) -eq 1 ]
  [ $(chk_develop_resource ATCG) -eq 1 ]
  [ $(chk_develop_resource 1.1) -eq 1 ]
  [ $(chk_develop_resource -30) -eq 1 ]
  [ $(chk_develop_resource 9) -eq 1 ]
  [ $(chk_develop_resource 3000) -eq 1 ]
}


@test "chk_build_resource" {

  [ $(chk_build_resource 30) -eq 0 ]
  [ $(chk_build_resource 300) -eq 0 ]

  [ $(chk_build_resource ) -eq 1 ]
  [ $(chk_build_resource XXYY) -eq 1 ]
  [ $(chk_build_resource 0.1) -eq 1 ]
  [ $(chk_build_resource -100) -eq 1 ]
  [ $(chk_build_resource 29) -eq 1 ]
  [ $(chk_build_resource 301) -eq 1 ]
}

##########
# Hensei
##########

@test "chk_shiplistno" {

  [ $(chk_shiplistno 101) -eq 0 ]
  [ $(chk_shiplistno "101") -eq 0 ]
  [ $(chk_shiplistno 110) -eq 0 ]
  [ $(chk_shiplistno 1010) -eq 0 ]
  [ $(chk_shiplistno 5010) -eq 0 ]
  [ $(chk_shiplistno 9901) -eq 0 ]

  [ $(chk_shiplistno ) -eq 1 ]
  [ $(chk_shiplistno -110) -eq 1 ]
  [ $(chk_shiplistno -010) -eq 1 ]
  [ $(chk_shiplistno 010) -eq 1 ]
  [ $(chk_shiplistno 100) -eq 1 ]
  [ $(chk_shiplistno 1111) -eq 1 ]
  [ $(chk_shiplistno x1111) -eq 1 ]
}

###################
# Expedition
###################

@test "chk_expd_id" {

  [ $(chk_expd_id 01) -eq 0 ]
  [ $(chk_expd_id "01") -eq 0 ]
  [ $(chk_expd_id 001) -eq 0 ]
  [ $(chk_expd_id 09) -eq 0 ]
  [ $(chk_expd_id 1) -eq 0 ]
  [ $(chk_expd_id 40) -eq 0 ]
  [ $(chk_expd_id A1) -eq 0 ]
  [ $(chk_expd_id "A1") -eq 0 ]
  [ $(chk_expd_id A3) -eq 0 ]
  [ $(chk_expd_id B1) -eq 0 ]

  [ $(chk_expd_id ) -eq 1 ]
  [ $(chk_expd_id 00) -eq 1 ]
  [ $(chk_expd_id -1) -eq 1 ]
  [ $(chk_expd_id 41) -eq 1 ]
  [ $(chk_expd_id 99) -eq 1 ]
  [ $(chk_expd_id 00) -eq 1 ]
  [ $(chk_expd_id A0) -eq 1 ]
  [ $(chk_expd_id a1) -eq 1 ]
  [ $(chk_expd_id A4) -eq 1 ]
  [ $(chk_expd_id B4) -eq 1 ]
}


@test "chk_expd_fleet_id" {

  [ $(chk_expd_fleet_id 2) -eq 0 ]
  [ $(chk_expd_fleet_id 3) -eq 0 ]
  [ $(chk_expd_fleet_id 4) -eq 0 ]

  [ $(chk_expd_fleet_id ) -eq 1 ]
  [ $(chk_expd_fleet_id 1) -eq 1 ]
  [ $(chk_expd_fleet_id 02) -eq 1 ]
  [ $(chk_expd_fleet_id 0) -eq 1 ]
  [ $(chk_expd_fleet_id 5) -eq 1 ]
  [ $(chk_expd_fleet_id 10) -eq 1 ]
  [ $(chk_expd_fleet_id -1) -eq 1 ]
  [ $(chk_expd_fleet_id A) -eq 1 ]
}

###################
# Sortie
###################

@test "chk_seaarea" {

  [ $(chk_seaarea 1) -eq 0 ]
  [ $(chk_seaarea "1") -eq 0 ]
  [ $(chk_seaarea 7) -eq 0 ]

  [ $(chk_seaarea ) -eq 1 ]
  [ $(chk_seaarea -1) -eq 1 ]
  [ $(chk_seaarea 01) -eq 1 ]
  [ $(chk_seaarea 8) -eq 1 ]
  [ $(chk_seaarea 10) -eq 1 ]
  [ $(chk_seaarea A) -eq 1 ]
}


@test "chk_map" {

  [ $(chk_map 1) -eq 0 ]
  [ $(chk_map "1") -eq 0 ]
  [ $(chk_map 6) -eq 0 ]

  [ $(chk_map ) -eq 1 ]
  [ $(chk_map -1) -eq 1 ]
  [ $(chk_map 01) -eq 1 ]
  [ $(chk_map 7) -eq 1 ]
  [ $(chk_map 10) -eq 1 ]
  [ $(chk_map A) -eq 1 ]
}


@test "chk_world" {

  [ $(chk_world 1 1) -eq 0 ]
  [ $(chk_world "1" "1") -eq 0 ]
  [ $(chk_world 01 1) -eq 0 ]
  [ $(chk_world 1 01) -eq 0 ]
  [ $(chk_world 1 3) -eq 0 ]
  [ $(chk_world 1 6) -eq 0 ]
  [ $(chk_world 2 1) -eq 0 ]
  [ $(chk_world 2 5) -eq 0 ]
  [ $(chk_world 6 5) -eq 0 ]
  [ $(chk_world 7 2) -eq 0 ]

  [ $(chk_world ) -eq 1 ]
  [ $(chk_world 1 0) -eq 1 ]
  [ $(chk_world 1 7) -eq 1 ]
  [ $(chk_world 2 6) -eq 1 ]
  [ $(chk_world 6 6) -eq 1 ]
  [ $(chk_world u u) -eq 1 ]
}


###################
# Combat
###################

@test "chk_formation_single" {

  [ $(chk_formation_single 1) -eq 0 ]
  [ $(chk_formation_single 01) -eq 0 ]
  [ $(chk_formation_single "01") -eq 0 ]
  [ $(chk_formation_single 5) -eq 0 ]
  # alert formation
  [ $(chk_formation_single 6) -eq 0 ]

  [ $(chk_formation_single) -eq 1 ]
  [ $(chk_formation_single 0) -eq 1 ]
  [ $(chk_formation_single 7) -eq 1 ]
}
