#!/usr/bin/env bash
#"""
# Test question.sh
#
# NOTE: no BATS due to interactive use
#"""

source "../src/question.sh"

#@test "question_ask test....please Enter 'y' 3 times!" {
tester() {
  local -a qs
  qs[0]="question 1"
  qs[1]="qUEsTioN 2 !"
  qs[2]="qUEsTioN 3 !"

  question_ask qs[@]
  # [ $? -eq 0 ]
}
tester
if (( $? == 0 )); then
  echo "$(date +%T) [Debug] Test OK!"
else
  echo "$(date +%T) [Error] Test Failed!"
  exit 1
fi
