#!/usr/bin/env bash
#"""
# Run all non-interactive test scripts
#"""

declare -a files=(
  "test_action.sh"
  "test_chkvar.sh"
  "test_expedition.sh"
  "test_func.sh"
)

for i in "${files[@]}"; do
  echo "==========================="
  echo "  running $i"
  echo "==========================="

  # NOTE: cannot use "command 2>&1 | tee out.txt"
  eval "./${i}"

  echo ""
done
