#!/usr/bin/env bats
#"""
# Test non-interactive functions within ``func.sh``
#
# Require `BATS test framework <https://github.com/sstephenson/bats>`_
#"""

# setup & teardown
setup() {
  source "../src/func.sh"
}


@test "ltnum_get_lt" {
  # Note zero-lead cases

  [ "$(ltnum_get_lt 1)" == "" ]
  [ "$(ltnum_get_lt 09)" == "" ]
  [ "$(ltnum_get_lt a099)" == "" ]
  [ "$(ltnum_get_lt ZZ999)" == "" ]

  [ "$(ltnum_get_lt A1)" == "A" ]
  [ "$(ltnum_get_lt A09)" == "A" ]
  [ "$(ltnum_get_lt A099)" == "A" ]
  [ "$(ltnum_get_lt Z999)" == "Z" ]
}


@test "ltnum_get_num" {

  [ $(ltnum_get_num A1) -eq 1 ]
  [ "$(ltnum_get_num A09)" == "9" ]
  [ "$(ltnum_get_num A099)" == "99" ]
  [ $(ltnum_get_num Z999) -eq 999 ]

  [ "$(ltnum_get_num 1)" == "" ]
  [ "$(ltnum_get_num 09)" == "" ]
  [ "$(ltnum_get_num a099)" == "" ]
  [ "$(ltnum_get_num ZZ999)" == "" ]
}

# takes a long time...since there are 2% cases....
@test "randsleepms: output range within [100%, 200%], repeat 500 times" {

  local basetime=100000
  local maxtime=$((basetime * 2))
  local num=0
  local errflag=0

  # loop
  local i=0
  while (( i < 500 )); do
    # generate
    num=$(randsleepms $basetime)
    # test
    if (( num < testtime || num > maxtime )); then
      errflag=1
      break
    fi
    (( i += 1 ))
  done

  # test
  [ $errflag -eq 0 ]
}
