#!/usr/bin/env bats
#"""
# Test pure mathematical functions in action.sh
#
# Require `BATS test framework <https://github.com/sstephenson/bats>`_
#"""

# setup & teardown
setup() {
  # const.sh, func.sh are sourced indirectly
  source "../src/action.sh"
}


@test "expd_cycle_sll_N_short makes sense" {

  # enumerate all practical cases
  [ $( expd_cycle_sll_N_short ${T_EXPD_A[1]} ${T_EXPD[37]} ${T_EXPD[38]} ) -eq 6 ]
  [ $( expd_cycle_sll_N_short ${T_EXPD_A[1]} ${T_EXPD[21]} ${T_EXPD[38]} ) -eq 5 ]
  [ $( expd_cycle_sll_N_short ${T_EXPD_A[1]} ${T_EXPD[21]} ${T_EXPD[37]} ) -eq 5 ]

  [ $( expd_cycle_sll_N_short ${T_EXPD[3]} ${T_EXPD[37]} ${T_EXPD[38]} ) -eq 7 ]
  [ $( expd_cycle_sll_N_short ${T_EXPD[3]} ${T_EXPD[21]} ${T_EXPD[38]} ) -eq 6 ]
  [ $( expd_cycle_sll_N_short ${T_EXPD[3]} ${T_EXPD[21]} ${T_EXPD[37]} ) -eq 6 ]

  [ $( expd_cycle_sll_N_short ${T_EXPD[2]} ${T_EXPD[37]} ${T_EXPD[38]} ) -eq 5 ]
  [ $( expd_cycle_sll_N_short ${T_EXPD[2]} ${T_EXPD[21]} ${T_EXPD[38]} ) -eq 4 ]
  [ $( expd_cycle_sll_N_short ${T_EXPD[2]} ${T_EXPD[21]} ${T_EXPD[37]} ) -eq 4 ]

  [ $( expd_cycle_sll_N_short ${T_EXPD[6]} ${T_EXPD[37]} ${T_EXPD[38]} ) -eq 3 ]
  [ $( expd_cycle_sll_N_short ${T_EXPD[6]} ${T_EXPD[21]} ${T_EXPD[38]} ) -eq 3 ]
  [ $( expd_cycle_sll_N_short ${T_EXPD[6]} ${T_EXPD[21]} ${T_EXPD[37]} ) -eq 3 ]

  [ $( expd_cycle_sll_N_short ${T_EXPD[5]} ${T_EXPD[37]} ${T_EXPD[38]} ) -eq 1 ]
  [ $( expd_cycle_sll_N_short ${T_EXPD[5]} ${T_EXPD[21]} ${T_EXPD[38]} ) -eq 1 ]
  [ $( expd_cycle_sll_N_short ${T_EXPD[5]} ${T_EXPD[21]} ${T_EXPD[37]} ) -eq 1 ]

  # pattern 21 37 38 have 0 intermediate returns
  [ $( expd_cycle_sll_N_short ${T_EXPD[21]} ${T_EXPD[37]} ${T_EXPD[38]} ) -eq 0 ]
}
