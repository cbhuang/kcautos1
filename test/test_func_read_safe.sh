#!/usr/bin/env bash
#"""
# Test read_safe() in func.sh
#
# NOTE: no BATS due to interactive use
#"""

source "../src/func.sh"

# read_safe() does discard unwanted characters, and affects outer scope
test_read_safe_affects_outer_scope() {
  # variable in outer scope
  local x=0

  # inject unwanted input
  echo "Enter random unwanted lines of characters (within 5 secs):"
  sleep 5

  # read
  read_safe x "please enter 11 for x value"

  # test
  if (( x == 11 )); then
    echo "$(date +%T) [Info] test_read_safe_affects_outer_scope passed!"
    echo ""
  else
    echo "$(date +%T) [Error] Bad x value: $x"
    echo "$(date +%T) [Error] test_read_safe_affects_outer_scope Failed!"
    exit 1
  fi
}
test_read_safe_affects_outer_scope


# create an intermediate scope
read_safe_innerscope() {
  # inner scope x
  local x=1

  # do
  echo "$(date +%T) [Debug] inner x value before read_safe(): $x"
  read_safe $1 "$2"
  echo "$(date +%T) [Debug] inner x value after read_safe(): $x"
}

test_read_safe_closest_outer_scope_only() {
  # outer scope x
  local x=0

  # unwanted input
  echo "Enter random unwanted lines of characters (within 5 seconds):"
  sleep 5

  # do
  echo "$(date +%T) [Debug] outer x value before read_safe(): $x"
  read_safe_innerscope x "please enter 11 for x value"
  echo "$(date +%T) [Debug] outer x value after read_safe(): $x"

  # test
  if (( x == 0 )); then
    echo "$(date +%T) [Debug] test_read_safe_closest_outer_scope_only passed!"
  else
    echo "$(date +%T) [Error] Bad x value: $x"
    echo "$(date +%T) [Error] test_read_safe_closest_outer_scope_only Failed!"
    exit 1
  fi
}
test_read_safe_closest_outer_scope_only


# echoes only if all tests were passed
echo "======================================"
echo "$(date +%T) [Debug] All tests passed!"
echo "======================================"
