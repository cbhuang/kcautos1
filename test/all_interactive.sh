#!/usr/bin/env bash
#"""
# Run all interactive test scripts
#"""

declare -a files=(
  "test_func_read_safe.sh"
  "test_question.sh"
)

for i in "${files[@]}"; do
  echo "==========================="
  echo "  running $i"
  echo "==========================="

  eval "./$i"

  echo ""
done
