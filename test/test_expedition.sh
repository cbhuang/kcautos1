#!/usr/bin/env bats
#"""
# Test pure mathematical functions in expedition.sh
#
# Require `BATS test framework <https://github.com/sstephenson/bats>`_
#"""

# setup & teardown
setup() {
  source "../src/expedition.sh"
}

@test "expd_id_get_seaarea" {

  [ $(expd_id_get_seaarea 1) -eq 1 ]
  [ $(expd_id_get_seaarea 01) -eq 1 ]
  [ $(expd_id_get_seaarea 8) -eq 1 ]
  [ $(expd_id_get_seaarea A1) -eq 1 ]
  [ $(expd_id_get_seaarea A3) -eq 1 ]
  [ $(expd_id_get_seaarea B1) -eq 2 ]
  [ $(expd_id_get_seaarea 9) -eq 2 ]
  [ $(expd_id_get_seaarea 24) -eq 3 ]
  [ $(expd_id_get_seaarea 40) -eq 5 ]
}


@test "expd_id_get_time" {

  [ $(expd_id_get_time 1) -eq 900 ]
  [ $(expd_id_get_time 9) -eq 14400 ]
  [ $(expd_id_get_time 40) -eq 24600 ]
  [ $(expd_id_get_time A1) -eq 1500 ]
  [ $(expd_id_get_time A3) -eq 8100 ]
  [ $(expd_id_get_time "B1") -eq 2100 ]
}
