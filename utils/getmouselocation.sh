#!/usr/bin/env bash
#"""
# Obtain the first 4 parameters for zmcs() on game screen
#"""

# source config files
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"
source "${CONFHOME}/X.conf"


mousecoor() {
  #"""
  # Get mouse coordinates.
  #
  # Encapsulated to prevent variable contamination.
  #"""

  # For xdotool getmouselocation --shell
  local X
  local Y
  local WINDOW
  local SCREEN
  # read character
  local res

  # get topleft info
  read -p "Move mouse to target topleft and press enter..." res
  eval $(xdotool getmouselocation --shell)
  # position relative to window origin
  local X0_REL=$(( $X - $XORIG ))
  local Y0_REL=$(( $Y - $YORIG ))
  # scale to ZOOM = 100%
  local XBEGIN=$(( $X0_REL * 100 / $ZOOM ))
  local YBEGIN=$(( $Y0_REL * 100 / $ZOOM ))
  echo "XBEGIN=$XBEGIN"
  echo "YBEGIN=$YBEGIN"

  # get bottomright info
  read -p "Move mouse to target bottomright and press enter..." res
  eval $(xdotool getmouselocation --shell)
  # position relative to last result
  local XRNG_REL=$(( $X - $XORIG - $X0_REL ))
  local YRNG_REL=$(( $Y - $YORIG - $Y0_REL ))
  # scale to ZOOM = 100%
  local XRNG=$(( $XRNG_REL * 100 / $ZOOM ))
  local YRNG=$(( $YRNG_REL * 100 / $ZOOM ))
  echo "XRNG=$XRNG"
  echo "YRNG=$YRNG"

  # output for copy-paste
  echo "Parameters: $XBEGIN $XRNG $YBEGIN $YRNG"
}

# execute
mousecoor
