#!/usr/bin/env bash
#"""
# C/C++ gcc build command
#
# Args:
#    $1 (str): source name (relative to pwd)
#"""

# set name of output binary: name.c(pp) -> name.exe
TARGET=$( echo $1 | sed -E "s/^(.*)\.c(pp)?$/\1\.exe/" )

# build
gcc ./$1 -o ../bin/$TARGET
