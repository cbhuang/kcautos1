#!/usr/bin/env bash
#"""
# Interactive Setup Script
#"""

echo "$(date +%T) [Info] setup.sh begins..."


#"""
# Set Project Home
#"""

PRJHOME="$(pwd)"

# overwirte existing local configuration
if [ -e "conf/prj.conf.template" ]; then
  cp -f "conf/prj.conf.template" "conf/prj.conf"
else
  echo "$(date +%T) [ERROR] ./conf/prj.conf.template not found!"
  exit 1
fi

# Modify config file in-place
sed -i -r "s#^PRJHOME=(.*)#PRJHOME=\"${PRJHOME}\"#" "conf/prj.conf"

# read new project config
source "conf/prj.conf"
# NOTE: from now, use "${CONFHOME}/prj.conf" to reference this file


#"""
# set game window
#"""

# Use existing X.conf
flag=n
if [ -e "${CONFHOME}/X.conf" ]; then
  echo "Existing X.conf found:"
  echo $(cat "${CONFHOME}/X.conf" | sed '1,3d')
  read -p "Accpet?(n/y)" flag
fi

# reset if previous result is not accepted
while [ "$flag" != "y" ]; do

    ## 1. top-left corner
    read -p "Move cursor to the top-left corner of the game window and press Enter. Error within ~3 pixels is OK." e
    eval $(xdotool getmouselocation --shell)

    # confirm
    read -p "accept XORIG=${X}, YORIG=${Y}?(n/y) " flag

    # manual input
    if [ "$flag" != "y" ]; then
      flag2=n
      while [ "$flag2" != "y" ]; do
        read -p "Enter XORIG: " X
        read -p "Enter YORIG: " Y
        read -p "OK?(n/y) " flag2
      done
    fi

    ## 2. zoom
    flag=n
    while [ "$flag" != "y" ]; do
      echo ""
      read -p "Enter browser zoom %: " ZOOM
      read -p "OK?(n/y) " flag
    done

    ## 3. write to X.conf
    read -p "All OK, write to X.conf? (y/n) " flag

    if [[ $flag == "y" ]]; then
      # write to "${CONFHOME}/X.conf"
      echo "#!/usr/bin/env bash" > "${CONFHOME}/X.conf"
      echo "#     X window parameters" >> "${CONFHOME}/X.conf"
      echo "# auto-regen by setup.sh at $(date +'%Y-%m-%d %T')" >> "${CONFHOME}/X.conf"
      echo "XORIG=${X}" >> "${CONFHOME}/X.conf"
      echo "YORIG=${Y}" >> "${CONFHOME}/X.conf"
      echo "ZOOM=${ZOOM}" >> "${CONFHOME}/X.conf"
      echo "" >> "${CONFHOME}/X.conf"
    else
      echo -e "Do over....\n"
    fi
done


#"""
# Set Debug Variables
#"""

# read
flag=n
while [ "$flag" != "y" ]; do
  echo ""
  read -p "Enable randomized sleep time?(y/n) " DBG
  read -p "Sure?(n/y) " flag
done

# write
if [ "$DBG" == "n" ]; then
  sed -i -r "s/^DBG_RNDTIME=(.*)/DBG_RNDTIME=n/" "${CONFHOME}/prj.conf"
else
  sed -i -r "s/^DBG_RNDTIME=(.*)/DBG_RNDTIME=y/" "${CONFHOME}/prj.conf"
fi


#"""
# Copy User Alias
#"""

# Existing file
if [ -f "${SCRIPTHOME}/alias.sh" ]; then

  read -p "Existing user alias file (alias.sh) found. Delete? (n/y)" flag
  if [ "$flag" == "y" ]; then
    cp -f "${SCRIPTHOME}/alias.sh.default" "${SCRIPTHOME}/alias.sh"
    chmod +x "${SCRIPTHOME}/alias.sh"
    echo "$(date +%T) [Info] alias.sh was overwritten by default."
  else
    echo "$(date +%T) [Info] alias.sh untouched"
  fi

else
  cp "${SCRIPTHOME}/alias.sh.default" "${SCRIPTHOME}/alias.sh"
  chmod +x "${SCRIPTHOME}/alias.sh"
  echo "$(date +%T) [Info] Copied alias.sh from default."
fi


# All done!
echo "$(date +%T) [Info] setup.sh done!"
