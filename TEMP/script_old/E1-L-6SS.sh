#!/usr/bin/env bash
#"""
# 2017 summer E1-L 6SS 葛城掘り
#"""

# project settings
DIR_THIS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR_THIS}/../conf/prj.conf"

# action library
source "${SRCHOME}/action.sh"


#"""
# check
#"""

# safety questions
declare -a qs=(
  "Is Land base = single 97-931 (distance >= 5)?"
  "Is SS Repaired?"
  "All done?"
)
question_ask qs[@]

echo "$(date +%T) [Info] event/E1-L-6SS.sh begins..."
countdown 3

#"""
# Main Program
#"""


echo "$(date +%T) [Info] Entering event seaarea..."
sidebar_supply
supply_all
sidebar_room
room_spe
spe_sortie
# enter event seaarea
zmcs 681 72 431 27 1 $T_CLICK_INSTANT


echo "$(date +%T) [Info] Supply land base..."
# enter land base
zmcs 140 77 382 24 1 $T_CLICK_ANIMATE
# supply wing 1
sortie_landbase_supply_wing
# leave land base
zmcs 140 77 382 24 1 $T_CLICK_ANIMATE


echo "$(date +%T) [Info] Entering map E1..."
for i in {1..3}; do
  zmcs 558 153 154 58 1 $T_CLICK_ANIMATE
done
# decide
zmcs 605 161 430 30 1 $T_CLICK_API
# go
zmcs 540 148 425 21 1 7000


echo "$(date +%T) [Info] Setting land base aircraft targets..."
zmcs 595 8 273 10 1 $T_CLICK_API
zmcs 359 10 313 10 1 $T_CLICK_API
zmcs 18 149 50 18 1 $T_CLICK_API


echo "$(date +%T) [Info] Advancing to point B..."
sleep 16
combat_formation 5
echo "$(date +%T) [Debug] combat B (33s)..."
sleep 33
combat_no_yasen_no_drop


echo "$(date +%T) [Info] Advancing to point G..."
combat_advance n
sleep 15


echo "$(date +%T) [Info] Advancing to point J..."
zmcs 359 9 313 10 1 10000
combat_formation 5
echo "$(date +%T) [Debug] combat J (33s)..."
sleep 33
combat_no_yasen_no_drop


echo "$(date +%T) [Info] Advancing to point K..."
combat_advance y
map_compass
combat_formation 3
echo "$(date +%T) [Debug] combat K (28s)..."
sleep 28
click_safe $T_COMBAT_RANK_EXP
click_safe $T_CLICK_ANIMATE


echo "$(date +%T) [Info] Advancing to point L..."
combat_advance n
map_sleep_no_compass
combat_formation 1
echo "$(date +%T) [Debug] combat L (< 60s)..."
sleep 60
combat_yasen_unsure


echo "$(date +%T) [Info] Show drop"
room_factory
factory_disintegrate


echo "$(date +%T) [Info] event/E1-L-6SS.sh done!"
